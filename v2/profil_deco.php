<?php
session_start();
?>
<!DOCTYPE html>
<link rel="stylesheet" href="dist/css/bootstrap.css">
<link rel="stylesheet" href="dist/css/normalize.css">
<link rel="stylesheet" href="style.css">
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Déconnexion</title>
</head>
<body>
   <p>Voulez-vous vraiment vous déconnecter ?</p>
    <a href="profil_deco_confirmer.php">Oui</a>
    <a href="accueil.php">Non</a>
</body>
</html>