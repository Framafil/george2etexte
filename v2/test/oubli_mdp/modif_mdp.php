<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../../dist/css/normalize.css">
    <link rel="stylesheet" href="../../style.css">
    <title>Modif mot de passe</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "../Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include("../../parameters.php");
    
    $mdp1 = strip_tags(htmlspecialchars($_POST['mdpin']));
    $mdp2 = strip_tags(htmlspecialchars($_POST['confirmation']));
    
    if($mdp1 == $mdp2 && !empty($_POST['token'])){
        $manager = new UtilisatriceManager($bdd);
        $token = $_POST['token'];
        $identifiant = $manager->getUtilisatriceByToken($token);
        
        $hashmdp = password_hash($mdp1 . $salage, PASSWORD_DEFAULT);
        
        if(password_verify($mdp1 . $salage, $hashmdp)){
            $manager->updateMdp($identifiant, $hashmdp);
            $manager->updateToken($identifiant, "");
            echo "Modification réussie ! Maintenant, veuillez quitter cet onglet.";
        }
    } else {
        echo "Les deux mots de passe ne correspondent pas";
    }
    ?>
</body>
</html>