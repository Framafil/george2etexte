<?php
$option = intval($_GET['a']);

if($option == -1){
    echo "<label for=\"prenom_autrice\">Prénom de l'autrice</label><input type=\"text\" name=\"prenom_autrice\" id=\"prenom_autrice\" required class=\"form-control\"><br><br>
<label for=\"nom_autrice\">Nom de l'autrice</label><input type=\"text\" name=\"nom_autrice\" id=\"nom_autrice\" required class=\"form-control\"><br><br>
<p>Sexe: </p>
<label for=\"masculin\">Masculin</label><input type=\"radio\" name=\"sexe_autrice\" id=\"masculin\" value=\"0\" required>

<label for=\"feminin\">Féminin</label><input type=\"radio\" name=\"sexe_autrice\" id=\"feminin\" value=\"1\" required><br><br>

<label for=\"naissance\">Année de naissance</label><input type=\"number\" name=\"naissance\" id=\"naissance\" required class=\"form-control\"><br>

<label for=\"nstr\">Pas sûr(e) de la date ?</label><input type=\"checkbox\" name=\"nstr\" id=\"nstr\" value=\"1\"><br><br>

<label for=\"deces\">Année de décès (falcultatif)</label><input type=\"number\" name=\"deces\" id=\"deces\" class=\"form-control\"><br>

<label for=\"dstr\">Pas sûr(e) de la date ?</label><input type=\"checkbox\" name=\"dstr\" id=\"dstr\" value=\"1\"><br><br>

<label for=\"biographie\">Biographie de l'auteur (falcultatif): </label><textarea name=\"biographie\" id=\"biographie\" class=\"form-control\" cols=\"30\" rows=\"10\" placeholder=\"Biographie...\"></textarea><br><br>

<p>Choisissez une photo de l'auteur: (falcultatif)</p>
<input type=\"file\" name=\"photo\" id=\"photo\" accept=\"image/jpeg\"><br><br>

<label for=\"lien_image\">Adresse source de la photo (falcultatif)</label><input type=\"url\" name=\"lien_image\" id=\"lien_image\" class=\"form-control\">";
}
?>