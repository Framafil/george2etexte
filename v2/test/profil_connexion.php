<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../dist/css/normalize.css">
    <link rel="stylesheet" href="../style.css">
    <title>Connexion vérification</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include('../parameters.php');
    
    $manager = new UtilisatriceManager($bdd);
    $mail = $_POST['mail'];
    $mdp = $_POST['mdp'];
    $verif = $manager->getUtilisatriceByMailMdp2($mail);
    
    if(password_verify($mdp . $salage, $verif)){
        $id = $manager->getUtilisatriceByMailMdp($mail, $verif);
        if($id !== 0){
            $utilisatrice = $manager->getUtilisatriceById($id);
        echo "<p>Bienvenue ".$utilisatrice->getPrenom_utilisatrice()." ".$utilisatrice->getNom_utilisatrice()."</p>";
            $_SESSION['id'] = $utilisatrice->getId_utilisatrice();
            echo "<a href=\"../index.php\">Continuer</a>";
        } else {
            echo "<p>Le mail n'existe pas</p><br>";
            echo "<a href=\"profil_connex_insc.php\">Retour</a>";
        }
    } else {
        echo "<p>Le mot de passe n'est pas bon</p><br>";
        echo "<a href=\"profil_connex_insc.php\">Retour</a>";
    }
    ?>
</body>
</html>