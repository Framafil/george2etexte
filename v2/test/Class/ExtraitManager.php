<?php

class ExtraitManager{
    
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function getAllExtraits(){
        $recherche = $this->db->query('SELECT * FROM 2etexte_utilisatrice, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice 
WHERE 2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice  AND 2etexte_extrait.ok_extrait = 1 ORDER BY 2etexte_extrait.date_modif_extrait DESC LIMIT 0,10');
        return $recherche;
    }
    
    public function getAllExtraitsByParameters($utilisatrice, $autrice, $joursansE){
        $requete = "SELECT * FROM 2etexte_utilisatrice, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice
WHERE 2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice 
AND 2etexte_extrait.ok_extrait = 1";
        
        if($utilisatrice !== 0){
            $utilisatrice_extrait = " AND 2etexte_extrait.utilisatrice_extrait=".intval($utilisatrice);
            $requete .= $utilisatrice_extrait;
        }
        
        if($autrice !== 0){
            $id_autrice = " AND 2etexte_autrice.id_autrice=".intval($autrice);
            $requete .= $id_autrice;
        }
        
        if($joursansE !== 0){
            $jourSansE = " AND (2etexte_autrice.nom_autrice='Cixous' OR 2etexte_autrice.nom_autrice='Duras' OR 2etexte_autrice.nom_autrice='Goby' OR 2etexte_autrice.nom_autrice='Pozzi' OR 2etexte_autrice.nom_autrice='Sand')";
            $requete .= $joursansE;
        }
        
        $requete .= " GROUP BY id_extrait ORDER BY 2etexte_extrait.date_modif_extrait DESC";
        
        
        
        $recherche = $this->db->query($requete);
        return $recherche;
    }
    
    
    
    public function getAllExtraitsByRegex($regtor){
        
        $reg = trim($regtor);
        
        $tablecaracteres = ['a', 'e', 'i', 'o', 'u', 'y', 'n', 'c', "'", '"'];
        $remplacement = ["(A|a|ä|â|à)", "(é|E|e|è|ê|ë)", "(I|i|î|ï)", "(O|o|ô|ö)", "(U|u|û|ü)", "(Y|y|ÿ)", "(N|n|ñ)", "(C|c|ç)", "(&lsquo;|&rsquo;|&ldquo;|&rdquo;)", "(&quot;|&laquo;&nbsp;|&raquo;&nbsp;)"];
        
        $caracspec = ['*', '+', '?', '(', ')', '|', '^', '$', '[', ']'];
        if(strlen($reg) >= 3){
            $dernierchar = trim(strip_tags(substr($reg, -1)));
        $premierchar = trim(strip_tags(substr($reg, 0, 1)));
        $reste = trim(strip_tags(substr($reg, 1, -1)));
            
            $regex = trim(str_replace($caracspec, '', $reste));
            $regex = trim(str_replace($tablecaracteres, $remplacement, $regex));
        
            $regex2 = trim(str_replace($caracspec, '', $dernierchar));
        $regex2 = trim(str_replace($tablecaracteres, $remplacement, $regex2));
        
            $regex3 = trim(str_replace($caracspec, '', $premierchar));
        $regex3 = trim(str_replace($tablecaracteres, $remplacement, $regex3));
            
            
            if(!empty($regex) && !empty($regex2) && !empty($regex3)){
                $regexultime = '^('.$regex3.'|.*([[:punct:]]|[[:space:]])'.$regex3.')'.$regex.'('.$regex2.'|'.$regex2.'([[:punct:]]|[[:space:]]).*)$';
            } else {
                $regexultime = "";
            }
            
        } else {
            $regex = trim(str_replace($caracspec, '', $reg));
            $regex = trim(strip_tags(str_replace($tablecaracteres, $remplacement, $regex)));
            
            if(!empty($regex)){
                $regexultime = '^'.$regex.'';
            } else {
                $regexultime = "";
            }
        }
        
        if(!empty($regexultime)){
            $requete = 'SELECT
  *
FROM
  2etexte_utilisatrice,
  2etexte_extrait,
  2etexte_oeuvre,
  2etexte_autrice
WHERE
  2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait
  AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre
  AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice
  AND 2etexte_extrait.ok_extrait = 1
  AND (
    2etexte_oeuvre.reference_oeuvre REGEXP "'.$regexultime.'"
    OR 2etexte_oeuvre.autrice_oeuvre REGEXP "'.$regexultime.'"
    OR 2etexte_extrait.titre_extrait REGEXP "'.$regexultime.'"
    OR 2etexte_oeuvre.autrice_oeuvre REGEXP "'.$regexultime.'"
    OR 2etexte_utilisatrice.prenom_utilisatrice REGEXP "'.$regexultime.'"
    OR 2etexte_utilisatrice.nom_utilisatrice REGEXP "'.$regexultime.'"
    OR CONCAT(
      2etexte_utilisatrice.prenom_utilisatrice,
      " ",
      2etexte_utilisatrice.nom_utilisatrice
    ) REGEXP "'.$regexultime.'"
    OR CONCAT(
      2etexte_utilisatrice.prenom_utilisatrice,
      "",
      2etexte_utilisatrice.nom_utilisatrice
    ) REGEXP "'.$regexultime.'"
    
    OR 2etexte_oeuvre.annee_oeuvre REGEXP "'.$regexultime.'"
  )
ORDER BY
2etexte_autrice.sexe DESC,
  2etexte_extrait.date_modif_extrait DESC
';
        
            $recherche = $this->db->query($requete);
        return $recherche;
        }
        
    }
    
    public function getExtraitDonneesById($id){
        $recherche = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice 
WHERE 2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice 
AND 2etexte_extrait.id_extrait = :id');
        $recherche->execute(array('id' => $id));
        $donnees = $recherche->fetch();
        return $donnees;
    }
    
    public function getUtilisatriceOfExtrait($id){
        $recherche = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice 
WHERE 2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice 
AND 2etexte_extrait.id_extrait = :id');
        $recherche->execute(array('id' => $id));
        $donnees = $recherche->fetch();
        return $donnees['utilisatrice_extrait'];
    }
    
    public function getTagsByExtrait($id){
        $recherche = $this->db->prepare('SELECT * FROM 2etexte_extrait, 2etexte_contient_tag, 2etexte_tag WHERE 2etexte_extrait.id_extrait = 2etexte_contient_tag.id_contient_tag_extrait AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_extrait.id_extrait = :id ORDER BY 2etexte_tag.nom_tag');
        $recherche->execute(array('id' => $id));
        $nbrelignes = $recherche->rowCount();
        
        $chaine = "";
        
        if($nbrelignes !== 0){
            while($donnees = $recherche->fetch()){
                switch($donnees['type_tag']){
                    case 0:
                        $classe = "tag0";
                        break;
                    case 1:
                        $classe = "tag1";
                        break;
                    case 2:
                        $classe = "tag2";
                        break;
                    case 3:
                        $classe = "tag3";
                        break;
                }
                $chaine .= "<li class=\"".$classe."\">".$donnees['nom_tag']."</li>&nbsp;";
            }
            return $chaine;
        } else {
            return "<p>Aucun tag</p>";
        }
    }
    
    public function getAutriceByExtrait($id_autrice){
        $recherche = $this->db->prepare("SELECT * FROM 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice 
        WHERE 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_autrice.id_autrice = :id");
        $recherche->execute(array('id' => $id_autrice));
        $donnees = $recherche->fetch();
        return $donnees;
    }
    
    public function accueilExtraits($displayUser){
        $recherche = $this->getAllExtraits();
        echo '<table class="table table-striped">
     <tr><th>Titre de l’extrait</th><th>&OElig;uvre</th>';
    
    if($displayUser == 0){
      echo '<th>Proposé par :</th></tr>';
   }
   
   // Affichage de tous les extraits
   while($data = $recherche->fetch()){
   
   
      // Titre de l'extrait
   echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td>";
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<td>";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</td>";
   
   // Informations sur la personne qui a proposé l'extrait.
       
       if($displayUser == 0){
      echo "<td><a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a></td></tr>";
   }
       
   }
    
    

   echo "</table>";
    echo "<a href=\"liste_extraits.php\">Voir des extraits plus anciens</a><br>";
    }
    
    public function getAllExtraitsExpert($tag1, $tag2, $tag3, $tag4){
        
        if(!empty($tag1) && !empty($tag2) && !empty($tag3) && !empty($tag4)){
            $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_contient_periode, 2etexte_extrait, 2etexte_oeuvre, 2etexte_utilisatrice, 2etexte_autrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tag1." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tag2." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tag3." AND EXISTS (
            SELECT *
            FROM 2etexte_contient_tag, 2etexte_extrait
            WHERE 2etexte_contient_tag.id_contient_tag_tag = ".$tag4." AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait
            ) AND 2etexte_extrait.ok_extrait = 1";  
        $recherche = $this->db->query($requete);
            
        } else if(!empty($tag1) || !empty($tag2) || !empty($tag3) || !empty($tag4)) {
            
            if(!empty($tag1)){
                $tagverif1 = $tag1;
            } else {
                $tagverif1 = "";
            }
            
            if(!empty($tag2)){
                $tagverif2 = $tag2;
            } else {
                $tagverif2 = "";
            }
            
            if(!empty($tag3)){
                $tagverif3 = $tag3;
            } else {
                $tagverif3 = "";
            }
            
            if(!empty($tag4)){
                $tagverif4 = $tag4;
            } else {
                $tagverif4 = "";
            }
            
            $tableau = [$tagverif1, $tagverif2, $tagverif3, $tagverif4];
            
            switch($tableau){
                case empty($tableau[2]) && !empty($tableau[0]) && !empty($tableau[1]) && !empty($tableau[3]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_contient_tag, 2etexte_contient_periode, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[1]." AND EXISTS(
                        SELECT *
            FROM 2etexte_contient_tag, 2etexte_extrait
            WHERE 2etexte_contient_tag.id_contient_tag_tag = ".$tableau[3]." AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait
                    ) AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && !empty($tableau[0]) && !empty($tableau[1]) && !empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_contient_periode, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[1]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[1]) && !empty($tableau[0]) && !empty($tableau[2]) && !empty($tableau[3]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_contient_periode, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[0]) && !empty($tableau[3]) && !empty($tableau[1]) && !empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND EXISTS(
                    SELECT *
            FROM 2etexte_contient_tag, 2etexte_extrait
            WHERE 2etexte_contient_tag.id_contient_tag_tag = ".$tableau[1]." AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait
                    ) AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[0]) && empty($tableau[1]) && !empty($tableau[2]) && !empty($tableau[3]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[2]) && empty($tableau[0]) && !empty($tableau[1]) && !empty($tableau[3]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_contient_tag, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND EXISTS(
                    SELECT *
            FROM 2etexte_contient_tag, 2etexte_extrait
            WHERE 2etexte_contient_tag.id_contient_tag_tag = ".$tableau[1]." AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait
                    ) AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && empty($tableau[0]) && !empty($tableau[1]) && !empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_notion, 2etexte_contient_tag, 2etexte_contient_notion, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[1]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[2]) && empty($tableau[1]) && !empty($tableau[0]) && !empty($tableau[3]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_contient_tag, 2etexte_contient_periode, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && empty($tableau[1]) && !empty($tableau[0]) && !empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_notion, 2etexte_contient_notion, 2etexte_contient_periode, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[2]) && empty($tableau[3]) && !empty($tableau[1]) && !empty($tableau[0]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_tag, 2etexte_contient_tag, 2etexte_contient_periode, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[1]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && !empty($tableau[0]) && empty($tableau[1]) && empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_periode, 2etexte_contient_periode, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_periode.id_periode = 2etexte_contient_periode.id_contient_periode AND 2etexte_contient_periode.id_contient_extrait = 2etexte_extrait.id_extrait AND 2etexte_periode.id_periode = ".$tableau[0]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && empty($tableau[0]) && !empty($tableau[1]) && empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_contient_tag, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[1]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case !empty($tableau[3]) && empty($tableau[0]) && empty($tableau[1]) && empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_tag, 2etexte_contient_tag, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_tag.id_tag = 2etexte_contient_tag.id_contient_tag_tag AND 2etexte_contient_tag.id_contient_tag_extrait = 2etexte_extrait.id_extrait AND 2etexte_tag.id_tag = ".$tableau[3]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                case empty($tableau[3]) && empty($tableau[0]) && empty($tableau[1]) && !empty($tableau[2]):
                    $requete = "SELECT DISTINCT * FROM 2etexte_notion, 2etexte_contient_notion, 2etexte_extrait, 2etexte_autrice, 2etexte_oeuvre, 2etexte_utilisatrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_extrait.utilisatrice_extrait = 2etexte_utilisatrice.id_utilisatrice AND 2etexte_notion.id_notion = 2etexte_contient_notion.id_contient_notion_notion AND 2etexte_contient_notion.id_contient_notion_extrait = 2etexte_extrait.id_extrait AND 2etexte_notion.id_notion = ".$tableau[2]." AND 2etexte_extrait.ok_extrait = 1";
                    break;
                    
            }
         $recherche = $this->db->query($requete);   
        } else {
            $recherche = "";
        }
        return $recherche;
    }
    
    public function allTexts($displayUser, $autrice, $joursansE, $regex, $tags){
        // Tites des colonnes
    
        
        
        if($regex !== 0){
            $recherche = $this->getAllExtraitsByRegex($regex);
        } else if($tags !== array()){
            $recherche = $this->getAllExtraitsExpert($tags[0], $tags[1], $tags[2], $tags[3]);
        } else if(isset($displayUser) || isset($autrice) || isset($joursansE)) {
            $recherche = $this->getAllExtraitsByParameters($displayUser, $autrice, $joursansE);
        }
    
        
        
   echo '<table class="table table-striped">
     <tr><th>Titre de l’extrait</th><th>&OElig;uvre</th>';
   if($displayUser == 0){
      echo '<th>Proposé par :</th></tr>';
   }
   $nbTexts=0;
   
        if(!empty($recherche)){
            while($data = $recherche->fetch()){
   
   $nbTexts++;
   
       if($regex !== 0){
            echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."?reg=".$regex."\">".$data["titre_extrait"]."</a></td>";
       } else {
           echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td>";
       }
      // Titre de l'extrait
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<td>";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</td>";
   
   // Informations sur la personne qui a proposé l'extrait.
   if($displayUser == 0){
      echo "<td><a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a></td></tr>";
   }
       
   }

   echo "</table>";
   
   // Nombre d'extraits affichés
    if($nbTexts == 0){
        echo "<p>Votre recherche n'a pas abouti</p>";
    } else {
        if($nbTexts>1){
      $pluriel = "s";
   } else {
      $pluriel = "";
   }
   echo '<div style="text-align:right">&rarr; '.$nbTexts.' extrait'.$pluriel.' au total</div>';
    }
        }
   // Affichage de tous les extraits
   
    }
    
    public function displayText($id, $titleType){
        
        require 'UtilisatriceManager.php';
    include('../parameters.php');
        
        $managerU = new UtilisatriceManager($bdd);
        
        $data = $this->getExtraitDonneesById($id);
        
        $utilisatrice = $managerU->getUtilisatriceById($data['id_utilisatrice']);
        
        // Image de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo "<a href=\"./extraits.php?autId=".$data["id_autrice"]."\">";
   }
   if(strlen($data["image_autrice"])==0){
      $data["image_autrice"]="./autrices/autrice.jpg";
   }
   echo '<img class="roundedImageRight" src="'.$data["image_autrice"].'" alt="'.$data["nom_autrice"].'" title="'.$data["prenom_autrice"].' '.$data["nom_autrice"].' - Source de l\'image : '.$data["image_autrice_source"].'">';
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   
   // Titre de l'extrait
   echo "<".$titleType.">".$data["titre_extrait"]."</".$titleType.">";
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<p style=\"text-align:left\">Extrait de : ";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</p>";
   
   // Informations sur la personne qui a proposé l'extrait.
   echo "<p style=\"text-align:left\">Extrait proposé par : <a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".$utilisatrice->fullName()."</a></p><br/>";
   
   // Introduction de l'extrait
   if(strlen($data["intro_extrait"])>0){
      echo "<div style=\"text-align:left\"><b>À propos de cet extrait :</b><br/>";
      echo '<div class="jumbotron" style="text-align:justify">'.$data["intro_extrait"];
      // Licence de l'introduction
      echo '<br/><small>(licence <a href="';
      if($data["droits_intro_extrait"]=="by-nc-sa"){
         echo 'https://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons BY-NC-SA</a>';
      } else {
         echo 'https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons BY-SA</a>';
      }
      echo ", ".$utilisatrice->fullName().")</small>";
      echo "</div>";
   }
   
   // Texte de l'extrait
   echo "<div style=\"text-align:left\"><b>Texte de l'extrait</b> (<a href=\"".$data["source_extrait"]."\">source</a>) <b>:</b><br/>";
   echo '<div class="jumbotron" style="text-align:justify">'.$data["texte_extrait"].'</div>';
   echo "</div>";
   
   echo "<hr/>";
    }
    
    public function lastTexts($count){
        $cherche = $this->db->query('SELECT * FROM 2etexte_utilisatrice, 2etexte_extrait, 2etexte_oeuvre, 2etexte_autrice
   WHERE 2etexte_utilisatrice.id_utilisatrice = 2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait = 2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice
   AND `2etexte_autrice`.sexe = 1 AND 2etexte_extrait.ok_extrait = 1 
   GROUP BY id_extrait ORDER BY 2etexte_extrait.date_creation_extrait DESC LIMIT 0,'.intval($count));
   echo '<table class="table table-striped">';
   while($data = $cherche->fetch()){
      // Titre de l'extrait
   echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td></tr>";   
   }
   echo "</table>";
    }
    
    public function statExtrait(){
        $cherche = $this->db->query('SELECT * FROM 2etexte_extrait WHERE ok_extrait = 1');
        $cherche->execute();
        $stat = $cherche->rowCount();
        if($stat > 1){
            return "<li>".$stat." extraits postés par toute la communauté</li>";
        } else {
            return "<li>".$stat." extrait posté par toute la communauté</li>";
        }
    }
    
    public function insertExtrait(Extrait $extr, array $tags, $bdd){
        $insertion = $this->db->prepare('INSERT INTO 2etexte_extrait(titre_extrait, texte_extrait, intro_extrait, droits_intro_extrait, source_extrait, utilisatrice_extrait, oeuvre_extrait, date_creation_extrait, date_modif_extrait, ok_extrait) VALUES(:titre, :texte, :intro, :droits, :source, :utilisatrice, :oeuvre, :creation, :modif, :ok)');
        $insertion->execute(array('titre' => $extr->titre_extrait,
                                 'texte' => $extr->texte_extrait,
                                 'intro' => $extr->intro_extrait,
                                 'droits' => $extr->droits_intro_extrait,
                                 'source' => $extr->source_extrait,
                                 'utilisatrice' => $extr->utilisatrice_extrait,
                                 'oeuvre' => $extr->oeuvre_extrait,
                                 'creation' => $extr->date_creation_extrait,
                                 'modif' => $extr->date_modif_extrait,
                                 'ok' => $extr->ok_extrait));
        
        $id_extrait = $bdd->lastInsertId();
        
        for($i = 0; $i < sizeof($tags); $i++){
            if(!empty($tags[$i])){
                $id = $tags[$i];
            $insertag = $this->db->prepare('INSERT INTO 2etexte_contient_tag(id_contient_tag_extrait, id_contient_tag_tag) VALUES(:extrait, :tag)');
            $insertag->execute(array('extrait' => $id_extrait,
                                    'tag' => $id));
            } else {
                continue;
            }
        }
    }
}
?>