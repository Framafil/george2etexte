<?php
class Extrait{
    public $id_extrait;
    public $titre_extrait;
    public $texte_extrait;
    public $intro_extrait;
    public $droits_intro_extrait;
    public $source_extrait;
    public $utilisatrice_extrait;
    public $oeuvre_extrait;
    public $date_creation_extrait;
    public $date_modif_extrait;
    public $ok_extrait;
    
    public function __construct(array $donnees){
        foreach($donnees as $key=>$value){
            $this->$key = $value;
        }
    }
}
?>