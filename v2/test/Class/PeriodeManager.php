<?php
class PeriodeManager{
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function getAllPeriodes(){
        $recherche = $this->db->query('SELECT * FROM 2etexte_periode');
        while($donnees = $recherche->fetch()){
            echo "<option value=\"".$donnees['id_periode']."\">".$donnees['nom_periode']."</option>";
        }
    }
    
    public function getAllPeriodesDet(){
        $recherche = $this->db->query('SELECT * FROM 2etexte_periode');
        while($donnees = $recherche->fetch()){
            echo "<option value=\"".$donnees['id_periode']."\">".$donnees['nom_det_periode']."</option>";
        }
    }
}
?>