<?php
class Autrice{
    public $id_autrice;
    public $prenom_autrice;
    public $nom_autrice;
    public $sexe;
    public $naissance;
    public $deces;
    public $naissance_str;
    public $deces_str;
    public $minibio;
    public $image_autrice;
    public $image_autrice_source;
    public $id_bnf;
    
    public function __construct(array $donnees){
        foreach($donnees as $key=>$value){
            $this->$key = $value;
        }
    }
    
    public function fullName(){
        $prenom = $this->prenom_autrice;
        $nom = $this->nom_autrice;
        
        $result = $prenom;
   if(mb_substr($prenom,-1,1,"utf-8")=="'" or mb_substr($prenom,-1,1,"utf-8")=="’"){
     $result.= $nom;
   } else {
     $result.= " ".$nom;
   }
   if(strlen($nom)==0){
      $result = $prenom;
   }
   if(strlen($prenom)==0){
      $result = $nom;
   }
   return $result;
    }
}
?>