<?php
class Utilisatrice{
    private $id;
    private $prenom;
    private $nom;
    private $mail;
    private $presentation;
    private $lien;
    private $photo;
    private $institution;
    private $etablissement;
    private $academie;
    private $mdp;
    private $token;
    
    public function getId_utilisatrice() {
        return intval($this->id);
    }
    
    public function setId_utilisatrice($id) {
        $this->id = intval($id);
    }
    
    public function getPrenom_utilisatrice() {
        return $this->prenom;
    }
    
    public function setPrenom_utilisatrice($prenom) {
        $this->prenom = $prenom;
    }
    
    public function getNom_utilisatrice() {
        return $this->nom;
    }
    
    public function setNom_utilisatrice($nom) {
        $this->nom = $nom;
    }
    
    public function getMail_utilisatrice() {
        return $this->mail;
    }
    
    public function setMail_utilisatrice($mail) {
        $this->mail = $mail;
    }
    
    public function getPresentation_utilisatrice() {
        return $this->presentation;
    }
    
    public function setPresentation_utilisatrice($presentation) {
        $this->presentation = $presentation;
    }
    
    public function getLien_utilisatrice() {
        if(isset($this->lien) && !empty($this->lien)){
            return "<a target=\"_blank\" href=\"".$this->lien."\">".$this->lien."</a>";
        } else {
            return "Pas de site Internet pour le moment";
        }
    }
    
    public function getLien_utilisatrice2(){
        return $this->lien;
    }
    
    public function setLien_utilisatrice($lien) {
        $this->lien = $lien;
    }
    
    public function getPhoto_utilisatrice() {
        return $this->photo;
    }
    
    public function setPhoto_utilisatrice($photo) {
        $this->photo = $photo;
    }
    
    public function getAdresse_institutionnelle() {
        return $this->institution;
    }
    
    public function setAdresse_institutionnelle($institution) {
        $this->institution = $institution;
    }
    
    public function getEtablissement() {
        return $this->etablissement;
    }
    
    public function setEtablissement($etablissement) {
        $this->etablissement = $etablissement;
    }
    
    public function getAcademie() {
        return $this->academie;
    }
    
    public function setAcademie($academie) {
        $this->academie = $academie;
    }
    
    public function getMdp_utilisatrice() {
        return $this->mdp;
    }
    
    public function setMdp_utilisatrice($mdp) {
        $this->mdp = $mdp;
    }
    
    public function getToken_utilisatrice() {
        return $this->token;
    }
    
    public function setToken_utilisatrice($token) {
        $this->token = $token;
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
    
    public function __construct(array $donnees){
        $this->hydrate($donnees);
    }
    
    public function affiche(){
        return "<h1>".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."</h1>
        <div><img width=\"220\" height=\"300\" src=\"".$this->getPhoto_utilisatrice()."\" alt=\"Photo de ".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."\" title=\"Photo de ".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."\"></div>
        <p>Site Internet: ".$this->getLien_utilisatrice()."</p>
        <p>".$this->getPresentation_utilisatrice()."</p>";
    }
    
    public function afficheProfil(){
        return "<h1>Informations sur votre profil</h1>
        <p>Prénom et nom: ".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."</p>
        <p>Mail: ".$this->getMail_utilisatrice()."</p>
        <p>Présentation: ".$this->getPresentation_utilisatrice()."</p>
        <p>Site Internet: ".$this->getLien_utilisatrice()."</p>
        <div><img width=\"220\" height=\"300\" src=\"".$this->getPhoto_utilisatrice()."\" alt=\"Photo de ".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."\" title=\"Photo de ".$this->getPrenom_utilisatrice()." ".$this->getNom_utilisatrice()."\"></div>
        <p>Etablissement d'enseignement: ".$this->getEtablissement().", ".$this->getAdresse_institutionnelle()."</p>
        <p>Académie: ".$this->getAcademie()."</p>";
    }
    
    public function afficheFormulaire(){
        return "<label for=\"prenom\">Prénom</label><br><input type=\"text\" name=\"prenom\" id=\"prenom\" class=\"form-control-static\" required autofocus value=\"".$this->getPrenom_utilisatrice()."\"><br><br>
        <label for=\"nom\">Nom</label><br><input type=\"text\" name=\"nom\" id=\"nom\" class=\"form-control-static\" required value=\"".$this->getNom_utilisatrice()."\"><br><br>
        <label for=\"mailing\">Adresse mail</label><br><input type=\"email\" name=\"mailing\" id=\"mailing\" class=\"form-control-static\" required value=\"".$this->getMail_utilisatrice()."\"><br><br><div id=\"nomail\"></div><div id=\"prev\"></div>
        <label for=\"presentation\">Présentez-vous !</label><br><br>
           <textarea name=\"presentation\" id=\"presentation\" cols=\"30\" rows=\"10\" class=\"form-control\" placeholder=\"Présentez-vous brièvement...\" maxlength=\"500\">".$this->getPresentation_utilisatrice()."</textarea><br><br><span id=\"ecrit\"></span><br><br>
           <label for=\"lien_site\">Lien vers votre site web</label><br><input type=\"url\" name=\"lien_site\" id=\"lien_site\" class=\"form-control-static\" placeholder=\"Au début, http:// ou https://\" value=\"".$this->getLien_utilisatrice2()."\"><br><br><div id=\"nolien\"></div>
           <label for=\"institution\">Mail institutionnel</label><br><input type=\"text\" name=\"institution\" id=\"institution\" class=\"form-control-static\" value=\"".$this->getAdresse_institutionnelle()."\"><br><br>
           <label for=\"etablissement\">Etablissement</label><br><input type=\"text\" name=\"etablissement\" id=\"etablissement\" class=\"form-control-static\" value=\"".$this->getEtablissement()."\"><br><br>
           <label for=\"academie\">Académie</label><br><input type=\"text\" name=\"academie\" id=\"academie\" class=\"form-control-static\" value=\"".$this->getAcademie()."\"><br><br>
           <p class=\"profil\">Vous pouvez modifier votre photo de profil si vous le souhaitez: 
           <input type=\"file\" name=\"photo\" id=\"photo\" class=\"form-control-static\" accept=\"image/jpeg\">
           </p><br>
           <p>Ou vous pouvez générer un avatar aléatoire: </p>
           <input type=\"checkbox\" name=\"avatar\" id=\"avatar\" value=\"OK !\"><label for=\"avatar\">Je veux avoir un avatar aléatoire</label><br><br>";
    }
    
    public function fullName(){
        $prenom = $this->getPrenom_utilisatrice();
        $nom = $this->getNom_utilisatrice();
        
        $result = $prenom;
   if(mb_substr($prenom,-1,1,"utf-8")=="'" or mb_substr($prenom,-1,1,"utf-8")=="’"){
     $result.= $nom;
   } else {
     $result.= " ".$nom;
   }
   if(strlen($nom)==0){
      $result = $prenom;
   }
   if(strlen($prenom)==0){
      $result = $nom;
   }
   return $result;
        
    }
    
    public function displayUser(){
   echo "  <img class=\"roundedImage\" src=\"".$this->getPhoto_utilisatrice()."\" alt=\"Image de profil de ".$this->fullName()."\" title=\"Image de profil de ".$this->fullName()."\">";
   echo "  <div style=\"padding:5px;text-align:left;\">";
   echo "  Par <a href=\"".$this->getLien_utilisatrice2()."\">".$this->fullName()."</a><br/>";
   echo $this->getPresentation_utilisatrice()."<br/>";
   echo '  </div>';
}
    
}
?>