<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Test 3</title>
</head>
<body>
   <div id="resultats"></div>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function(){
            
            var idBnf, idIsni, idWikidata;
            
            $.getJSON("https://query.wikidata.org/sparql?query=SELECT%20DISTINCT%20%3Fautrice%20%3Ftravail%20%3FbnfId%20%3Fisni%20%3FwikiData%20%3Fnaissance%20(YEAR(%3Fnaissance)%20as%20%3FanneeNaissance)%20(YEAR(%3Fmort)%20as%20%3FanneeMort)%20%3Fmort%0AWHERE%20%7B%20%0A%20%20%7B%3FwikiData%20rdfs%3Alabel%20\"An\"%40fr.%7D%0A%20%20%7B%20%3FwikiData%20wdt%3AP106%20%3Fwork%20.%7D%0A%20%20%7B%20%3Fwork%20rdfs%3Alabel%20%3Ftravail%20filter%20(lang(%3Ftravail)%20%3D%20\"fr\").%20%7D%0A%20%20OPTIONAL%20%7B%20%3FwikiData%20wdt%3AP268%20%3FbnfId%20.%7D%20%0A%20%20OPTIONAL%20%7B%20%3FwikiData%20wdt%3AP213%20%3Fisni%20.%7D%20%0A%20%20OPTIONAL%20%7B%3FwikiData%20wdt%3AP569%20%3Fnaissance.%20%7D%20%0A%20%20OPTIONAL%20%7B%3FwikiData%20wdt%3AP570%20%3Fmort.%20%7D%20%0A%20%20%3FwikiData%20rdfs%3Alabel%20%3Fautrice%20filter%20(lang(%3Fautrice)%20%3D%20\"fr\").%20%0A%7D%20%0AGROUP%20BY%20%3FwikiData%20%3Ftravail%20%3Fautrice%20%3FbnfId%20%3Fisni%20%3Fnaissance%20%3Fmort%20ORDER%20BY%20ASC(%3Fnaissance)%20LIMIT%201&format=json", function(data){
                $.each(data.results.bindings, function(i, item){
                    
                    if(item.bnfId){
                        idBnf = item.bnfId.value;
                    } else {
                        idBnf = "Aucun";
                    }
                    
                    if(item.isni){
                        idIsni = item.isni.value;
                    } else {
                        idIsni = "Aucun";
                    }
                    
                    if(item.wikiData){
                        idWikidata = item.wikiData.value;
                    } else {
                        idWikidata = "Aucun";
                    }
                    
                    var enfantsP = $('#resultats').children('p');
                    
                    var nonInsert = 0;
                    
                    for(var i = 0; i < enfantsP.length; i++){
                        if(enfantsP[i].textContent == "Autrice: "+ item.autrice.value +", ID BNF: "+ idBnf +", ID ISNI: "+ idIsni +", Lien Wikidata: "+ idWikidata +""){
                            nonInsert++;
                            i = 0;
                            break;
                        } else {
                            nonInsert = 0;
                        }
                    }
                    
                    if(nonInsert == 0){
                        $("<p>Autrice: "+ item.autrice.value +", ID BNF: "+ idBnf +", ID ISNI: "+ idIsni +", Lien Wikidata: "+ idWikidata +"</p>").appendTo("#resultats");
                    } 
                });
            });
        });
    </script>
</body>
</html>