<?php

/* Affiche un article à partir de son identifiant
 *
 * @param $id: identifiant de l'article
 * @param $link: lien vers la base de données
 */
function displayArticle($id,$link){
   $id=intval($id);
   
   // Requête pour récupérer l'article à partir de son identifiant
   $sql = 'SELECT * FROM 2etexte_article,2etexte_utilisatrice,2etexte_autrice
   WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_article.id_utilisatrice_article
   AND 2etexte_article.ok_article=1 AND 2etexte_article.id_article='.$id.' ORDER BY 2etexte_article.date_creation_article DESC';
   $req = mysqli_query($link, $sql)
      or die('Erreur SQL !<br>');//.$sql.'<br>'.mysqli_error($link));
   $data = mysqli_fetch_assoc($req);
   
   // Titre de l'article
   echo "<h2><a href=\"./article.php?id=".$data["id_article"]."\">".$data["titre_article"]."</a></h2>";

   echo '<div class="panel-body">';
   // Information sur la contributrice
   echo '  <div class="col-lg-6 col-md-6">';
    $utilisatrice = new Utilisatrice($data);
   displayUser($utilisatrice);
   echo '  </div>';
   // Information sur la date de création de l'article
   echo '  <div class="col-lg-6 col-md-6" style="text-align:right;">';
   echo '  <p><i>Article ajouté le '.displayDate(new DateTime($data["date_creation_article"])).'</i></p>';
   echo '  </div>';   
   echo '</div>';
      
   // Introduction de l'article
   echo "<div class=\"texteArticle\">";
   echo $data["texte_article"];
   
   // Section "Extraits"
   echo "<hr/><h3>Les extraits de textes :</h3>";
   $sql2 = 'SELECT * FROM 2etexte_article,2etexte_extrait,2etexte_contient_extraits,2etexte_oeuvre,2etexte_autrice,2etexte_utilisatrice
   WHERE 2etexte_contient_extraits.id_contient_extrait_article=2etexte_article.id_article AND 2etexte_contient_extraits.id_contient_extrait_extrait=2etexte_extrait.id_extrait AND 2etexte_extrait.oeuvre_extrait=2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice AND 2etexte_extrait.utilisatrice_extrait=2etexte_utilisatrice.id_utilisatrice
   AND 2etexte_article.id_article='.$id.' ORDER BY 2etexte_contient_extraits.id_contient_extrait ASC';
   $req2 = mysqli_query($link, $sql2)
   or die('Erreur SQL !<br>');//.$sql2.'<br>'.mysqli_error($link));
   while($data2 = mysqli_fetch_assoc($req2)){
      displayText($data2,"h2");
   }

   // Section "Activités pédagogiques"
   echo "<h3>Activités pédagogiques :</h3>";
   echo $data["activite_article"];
   
   // Section "Pour approfondir"
   if ($data["ref_article"]!=""){
      echo "<h3>Pour approfondir :</h3>";
      echo $data["ref_article"];
   }
   
   // Copyright
   echo "<h3>Crédits :</h3>";
   if ($data["droits_article"]!="by-nc-sa"){
      echo "<p>Cet article de <a href=\"".$data["lien_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a> est mis à disposition sous licence Creative Commons <a href=\"https://creativecommons.org/licenses/by-sa/4.0/deed.fr\">Attribution - Partage dans les Mêmes Conditions 4.0 International (CC BY-SA 4.0)</a>, permettant sa réutilisation dans un cadre non-commercial sous réserve de maintenir cette licence et de citer son autrice ou auteur.</p>";
   } else {
      echo "<p>Cet article de <a href=\"".$data["lien_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a> est mis à disposition sous licence Creative Commons <a href=\"https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr\">Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0)</a>, permettant sa réutilisation dans un cadre non-commercial sous réserve de maintenir cette licence et de citer son autrice ou auteur.</p>";
   }
   
   echo "</div>";
}


/* Affiche un résumé d'article à partir de son identifiant
 *
 * @param $id: identifiant de l'article
 * @param $link: lien vers la base de données
 */
function displayArticleAbstract($id,$link){
   $id=intval($id);
   // Requête pour récupérer l'article à partir de son identifiant
   $sql = 'SELECT * FROM 2etexte_article,2etexte_utilisatrice,2etexte_autrice
   WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_article.id_utilisatrice_article
   AND 2etexte_article.ok_article=1 AND 2etexte_article.id_article='.$id.' ORDER BY 2etexte_article.date_creation_article DESC';
   $req = mysqli_query($link, $sql)
      or die('Erreur SQL !<br>');//.$sql.'<br>'.mysqli_error($link));
   $data = mysqli_fetch_assoc($req);

   // Titre de l'article
   echo "<h2><a href=\"./article.php?id=".$data["id_article"]."\">".$data["titre_article"]."</a></h2>";

   echo '<div class="panel-body">';
   // Information sur la contributrice
   echo '  <div class="col-lg-6 col-md-6">';
    $utilisatrice = new Utilisatrice($data);
   displayUser($utilisatrice);
   echo '  </div>';
   // Information sur la date de création de l'article
   echo '  <div class="col-lg-6 col-md-6" style="text-align:right;">';
   echo '  <p><i>Article ajouté le '.displayDate(new DateTime($data["date_creation_article"])).'</i></p>';
   echo '  </div>';   
   echo '</div>';
   
   // Information sur les autrices et auteurs des extraits associés
   $sql2 = 'SELECT * FROM 2etexte_article,2etexte_extrait,2etexte_contient_extraits,2etexte_oeuvre,2etexte_autrice,2etexte_utilisatrice
   WHERE 2etexte_contient_extraits.id_contient_extrait_article=2etexte_article.id_article AND 2etexte_contient_extraits.id_contient_extrait_extrait=2etexte_extrait.id_extrait AND 2etexte_extrait.oeuvre_extrait=2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice AND 2etexte_extrait.utilisatrice_extrait=2etexte_utilisatrice.id_utilisatrice
   AND 2etexte_article.ok_article=1 AND 2etexte_article.id_article='.$id.' ORDER BY 2etexte_contient_extraits.id_contient_extrait DESC';
   $req2 = mysqli_query($link, $sql2)
   or die('Erreur SQL !<br>');//.$sql2.'<br>'.mysqli_error($link));
   while($data2 = mysqli_fetch_assoc($req2)){
      echo "<a href=\"./extraits.php?autId=".$data2["id_autrice"]."\">";
      echo "<img class=\"roundedImageRight\" src=\"".$data2["image_autrice"]."\" alt=\"".$data2["nom_autrice"]."\">";
      echo "</a>";
   }
   
   // Début de l'introduction de l'article et lien vers l'article
   echo "<div class=\"texteArticle\">".mb_substr($data["texte_article"],0,600)."... ";
   echo "<a href=\"./article.php?id=".$data["id_article"]."\"><i><b>Lire la suite</b></i></a>";
   echo "</div>";
}


/* Affiche les informations sur une contributrice à partir des données de la base
 *
 * @param $data: données sur la contributrice
 */
function displayUser($util){
   echo "  <img class=\"roundedImage\" src=\"".$util->getPhoto_utilisatrice()."\" alt=\"Image de profil de ".fullName($util->getPrenom_utilisatrice(), $util->getNom_utilisatrice())."\" title=\"Image de profil de ".fullName($util->getPrenom_utilisatrice(), $util->getNom_utilisatrice())."\">";
   echo "  <div style=\"padding:5px;text-align:left;\">";
   echo "  Par <a href=\"".$util->getLien_utilisatrice2()."\">".fullName($util->getPrenom_utilisatrice(), $util->getNom_utilisatrice())."</a><br/>";
   echo $util->getPresentation_utilisatrice()."<br/>";
   echo '  </div>';
}


/* Affiche les informations sur une autrice à partir des données de la base
 *
 * @param $data: données sur l'autrice
 */
function displayAuthor($data){
    if(strlen($data["image_autrice"]) !== 0){
        echo "  <a href=\"".$data["image_autrice_source"]."\"><img class=\"roundedImage\" src=\"".$data["image_autrice"]."\" alt=\"Illustration de ".fullName($data["prenom_autrice"],$data["nom_autrice"])."\" title=\"Illustration de ".fullName($data["prenom_autrice"],$data["nom_autrice"])." - Source : ".$data["image_autrice_source"]."\"></a>";
    } else {
        echo "  <a href=\"".$data["image_autrice_source"]."\"><img class=\"roundedImage\" src=\"autrices/autrice.jpg\" alt=\"Illustration de ".fullName($data["prenom_autrice"],$data["nom_autrice"])."\" title=\"Illustration de ".fullName($data["prenom_autrice"],$data["nom_autrice"])." - Source : ".$data["image_autrice_source"]."\"></a>";
    }
   
   echo "  <div style=\"padding:5px;text-align:left;\">";
   echo "     <a href=\"http://data.bnf.fr/ark:/12148/".$data["id_bnf"]."#foaf:Person\"><big>".fullName($data["prenom_autrice"],$data["nom_autrice"])."</big></a><br/>(".$data["naissance_str"]."-".$data["deces_str"].")<br/>";
   echo '  </div>';
}


/* Affiche une date
 *
 * @param $date: date
 */
function displayDate($date){
   return date("d/m/Y",$date->getTimestamp()).' à '.date("H",$date->getTimestamp()).'h'.date("i",$date->getTimestamp());
}

/* Affiche un extrait à partir des données de la base
 *
 * @param $data: données sur l'extrait
 * @param $titleType: type de titre (h2, h3, etc.)
 */
function displayText($data,$titleType){

   // Image de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo "<a href=\"./extraits.php?autId=".$data["id_autrice"]."\">";
   }
   if(strlen($data["image_autrice"])==0){
      $data["image_autrice"]="./autrices/autrice.jpg";
   }
   echo '<img class="roundedImageRight" src="'.$data["image_autrice"].'" alt="'.$data["nom_autrice"].'" title="'.$data["prenom_autrice"].' '.$data["nom_autrice"].' - Source de l\'image : '.$data["image_autrice_source"].'">';
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   
   // Titre de l'extrait
   echo "<".$titleType.">".$data["titre_extrait"]."</".$titleType.">";
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<p style=\"text-align:left\">Extrait de : ";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</p>";
   
   // Informations sur la personne qui a proposé l'extrait.
   echo "<p style=\"text-align:left\">Extrait proposé par : <a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a></p><br/>";
   
   // Introduction de l'extrait
   if(strlen($data["intro_extrait"])>0){
      echo "<div style=\"text-align:left\"><b>À propos de cet extrait :</b><br/>";
      echo '<div class="jumbotron" style="text-align:justify">'.$data["intro_extrait"];
      // Licence de l'introduction
      echo '<br/><small>(licence <a href="';
      if($data["droits_intro_extrait"]=="by-nc-sa"){
         echo 'https://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons BY-NC-SA</a>';
      } else {
         echo 'https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons BY-SA</a>';
      }
      echo ", ".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"]).")</small>";
      echo "</div>";
   }
   
   // Texte de l'extrait
   echo "<div style=\"text-align:left\"><b>Texte de l'extrait</b> (<a href=\"".$data["source_extrait"]."\">source</a>) <b>:</b><br/>";
   echo '<div class="jumbotron" style="text-align:justify">'.$data["texte_extrait"].'</div>';
   echo "</div>";
   
   echo "<hr/>";
}


/* Nettoie une chaîne de caractères en supprimant certaines balises HTML et en remplaçant les apostrophes et guillemets droits
 *
 * @param $str: chaîne de caractères où seront faites les modifications
 */
function stringClean($str){
   $str = strip_tags($str,"<p><b><i><u><h1><h2><h3><h4><h5><div><span><em><strong><tt><del><small><big><sup><sub><var><table><th><td><tr><br><img><blockquote>");
   $str = str_replace("'","&rsquo;",$str);
   $str = str_replace("&apos;","&rsquo;",$str);
   $str = str_replace("&#039;","&rsquo;",$str);
   $str = str_replace("&#39;","&rsquo;",$str);
   $str = str_replace("\"","&rsquo;&rsquo;",$str);
   return $str;
}



/* Affiche les derniers extraits de textes de femmes soumis
 *
 * @param $count: nombre d'extraits à afficher
 */
function lastTexts($count,$link){
   $sql = 'SELECT * FROM 2etexte_utilisatrice,2etexte_extrait,2etexte_oeuvre,2etexte_autrice
   WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait=2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice
   AND `2etexte_autrice`.sexe=1 AND 2etexte_extrait.ok_extrait=1 
   GROUP BY id_extrait ORDER BY date_creation_extrait DESC LIMIT '.intval($count);
   $req = mysqli_query($link, $sql)
   or die('Erreur SQL !<br>');//.$sql.'<br>'.mysqli_error($link));
   echo '<table class="table table-striped">';
   while($data = mysqli_fetch_assoc($req)){
      // Titre de l'extrait
   echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td></tr>";   
   }
   echo "</table>";
}


/* Affiche tous les extraits de textes de femmes soumis
 *
 * @param $link: connexion à la base de données
 * @param $sql: sql query
 */

function accueilExtraits($recherche, $displayUser){
    echo '  <table class="table table-striped">
     <tr><th>Titre de l’extrait</th><th>&OElig;uvre</th>';
    
    if($displayUser == 0){
      echo '<th>Proposé par :</th></tr>';
   }
   
   // Affichage de tous les extraits
   while($data = $recherche->fetch()){
   
   
      // Titre de l'extrait
   echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td>";
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<td>";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</td>";
   
   // Informations sur la personne qui a proposé l'extrait.
       
       if($displayUser == 0){
      echo "<td><a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a></td></tr>";
   }
       
   }
    
    

   echo "</table>";
    echo "<a href=\"liste_extraits.php\">Voir des extraits plus anciens</a><br>";
}


function allTexts($recherche, $displayUser, $regex){

   // Tites des colonnes
    
        
   echo '  <table class="table table-striped">
     <tr><th>Titre de l’extrait</th><th>&OElig;uvre</th>';
   if($displayUser == 0){
      echo '<th>Proposé par :</th></tr>';
   }
   $nbTexts=0;
   
   // Affichage de tous les extraits
   while($data = $recherche->fetch()){
   
   $nbTexts++;
   
       if($regex !== 0){
            echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."?reg=".$regex."\">".$data["titre_extrait"]."</a></td>";
       } else {
           echo "<tr><td><a href=\"extrait.php?id=".$data["id_extrait"]."\">".$data["titre_extrait"]."</a></td>";
       }
      // Titre de l'extrait
   
   // Informations sur l'oeuvre d'où est tiré l'extrait
   echo "<td>";
   
   // Nom de l'autrice
   if(strlen($data["id_bnf"])>0){
      echo '<a href="./extraits.php?autId='.$data["id_autrice"].'">';
   }
   if(strlen($data["autrice_oeuvre"])>0){
      echo $data["autrice_oeuvre"];
   }
   if(strlen($data["id_bnf"])>0){
      echo "</a>";
   }
   echo ", ";
   
   // Titre de l'oeuvre
   if(strlen($data["source_oeuvre"])>0){
      echo "<a href=\"".$data["source_oeuvre"]."\">";
   }
   echo "<i>".$data["reference_oeuvre"]."</i>";
   if(strlen($data["source_oeuvre"])>0){
      echo "</a>";
   }
   
   // Année de publication
   if(strlen($data["annee_oeuvre"])>0){
      echo ", ".$data["annee_oeuvre"];
   }
   
   // Lien d'achat
   if(strlen($data["achat_oeuvre"])>0){
      echo " <small>(<a href=\"".$data["achat_oeuvre"]."\">acheter l&rsquo;&oelig;uvre</a>)</small>";
   }
   echo "</td>";
   
   // Informations sur la personne qui a proposé l'extrait.
   if($displayUser == 0){
      echo "<td><a href=\"extraits.php?id=".$data["id_utilisatrice"]."\" title=\"".$data["presentation_utilisatrice"]."\">".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"])."</a></td></tr>";
   }
       
   }

   echo "</table>";
   
   // Nombre d'extraits affichés
    if($nbTexts == 0){
        echo "<p>Votre recherche n'a pas abouti</p>";
    } else {
        if($nbTexts>1){
      $pluriel = "s";
   } else {
      $pluriel = "";
   }
   echo '<div style="text-align:right">&rarr; '.$nbTexts.' extrait'.$pluriel.' au total</div>';
    }

}


/* Affiche les derniers articles soumis
 *
 * @param $count: nombre d'articles à afficher
 */
function lastArticles($count,$link){
   $sql = 'SELECT * FROM 2etexte_utilisatrice,2etexte_article
   WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_article.id_utilisatrice_article
   AND 2etexte_article.ok_article=1 
   GROUP BY id_article ORDER BY date_creation_article DESC LIMIT '.intval($count);
   $req = mysqli_query($link, $sql)
   or die('Erreur SQL !<br>');//.$sql.'<br>'.mysqli_error($link));
   echo '<table class="table table-striped">';
   while($data = mysqli_fetch_assoc($req)){
      // Titre de l'extrait
   echo "<tr><td><a href=\"article.php?id=".$data["id_article"]."\">".$data["titre_article"]."</a></td></tr>";   
   }
   echo "</table>";
}


/* Renvoie le nom complet en évitant de mettre un espace si le nom de famille est précédé d'une apostrophe
 *
 * @param $firstName: the first name, possibily ending with ' or ’
 * @param $lastName: the last name
 */
function fullName($firstName, $lastName){
   $result=$firstName;
   if(mb_substr($firstName,-1,1,"utf-8")=="'" or mb_substr($firstName,-1,1,"utf-8")=="’"){
     $result.=$lastName;
   } else {
     $result.=" ".$lastName;
   }
   if(strlen($lastName)==0){
      $result=$firstName;
   }
   if(strlen($firstName)==0){
      $result=$lastName;
   }
   return $result;
}
?>