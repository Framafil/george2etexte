<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="../style.css" />
   <link href="../dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="../images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("parameters.php");
include("functions.php");
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    
    date_default_timezone_set('Europe/Paris');

    $managerE = new ExtraitManager($bdd);
    $managerO = new OeuvreManager($bdd);
    $managerA = new AutriceManager($bdd);
    $managerT = new TagManager($bdd);

    $titre_extrait = stringClean(strip_tags(htmlspecialchars($_POST["titre_extrait"])));

    if(isset($_POST['texte_extrait']) && !empty($_POST['texte_extrait'])){
        $texte_extrait = stringClean($_POST["texte_extrait"]);
    } else {
        $texte_extrait = "<p>Aïe, il n'y pas d'extrait. Dommage.</p>";
    }
    

if(isset($_POST["intro_extrait"])) {
    $intro_extrait = stringClean($_POST["intro_extrait"]);
} else {
    $intro_extrait = "";
}

    $droits_intro_extrait = stringClean($_POST["droits_intro_extrait"]);

if(isset($_POST["source_extrait"])) {
    $source_extrait = stringClean($_POST["source_extrait"]);
} else {
    $source_extrait = "";
}

$utilisatrice_extrait = intval($_SESSION['id']);
    
    if(isset($_POST['id_autrice']) && !empty($_POST['id_autrice'])){
        $id_aut = intval($_POST['id_autrice']);
    } else {
        $id_aut = "";
    }
    
    if(isset($_POST['id_oeuvre']) && !empty($_POST['id_oeuvre'])){
        $id_oeu = intval($_POST['id_oeuvre']);
    } else {
        $id_oeu = "";
    }
    
    if(isset($_POST['prenom_autrice'])){
        $prenom_autrice = strip_tags(htmlspecialchars($_POST['prenom_autrice']));
    } else {
        $prenom_autrice = "";
    }
    
    if(isset($_POST['nom_autrice'])){
        $nom_autrice = strip_tags(htmlspecialchars($_POST['nom_autrice']));
    } else {
        $nom_autrice = "";
    }
    
    if(isset($_POST['sexe_autrice'])){
        $sexe = intval($_POST['sexe_autrice']);
    } else {
        $sexe = NULL;
    }
    
    if(isset($_POST['naissance'])){
        $naissance = intval($_POST['naissance']);
        if(isset($_POST['nstr']) && intval($_POST['nstr']) == 1){
        $nstr = "".$naissance."?";
    } else {
            $nstr = "".$naissance."";
        }
    } else {
        $naissance = "";
        $nstr = "";
    }
    
    if(isset($_POST['deces']) && !empty($_POST['deces'])){
        $deces = intval($_POST['deces']);
        if(isset($_POST['dstr']) && intval($_POST['dstr']) == 1){
            $dstr = "".$deces."?";
        } else {
            $dstr = "".$deces."";
        }
    } else {
        $deces = NULL;
        $dstr = "";
    }
    
    if(isset($_POST['biographie'])){
        $biographie = strip_tags(htmlspecialchars($_POST['biographie']));
    } else {
        $biographie = "";
    }
    if(isset($_FILES['photo']['name']) && !empty($_FILES['photo']['name'])){
        $tmp_file = $_FILES['photo']['tmp_name'];
        if(is_uploaded_file($tmp_file)){
            $newfile = explode(".", $_FILES['photo']['name']);
            $hashfile = hash("sha256", $newfile[0]);
            $fichier = "autrices/" . $hashfile . "." . $newfile[1];
            move_uploaded_file($tmp_file, $fichier);
            $image_autrice = $fichier;
        }
    } else {
       $image_autrice = "autrices/autrice.jpg"; 
    }
    
    if(isset($_POST['lien_image'])){
        $image_source = strip_tags(htmlspecialchars($_POST['lien_image']));
    } else {
        $image_source = "";
    }
    
    if(isset($_POST['id_bnf']) && !empty($_POST['id_bnf'])){
        $id_bnf = $_POST['id_bnf'];
    } else {
        $id_bnf = "";
    }
    
    if(isset($_POST['id_isni']) && !empty($_POST['id_isni'])){
        $id_isni = $_POST['id_isni'];
    } else {
        $id_isni = "";
    }
    
    if(isset($_POST['id_wikidata']) && !empty($_POST['id_wikidata'])){
        $id_wikidata = $_POST['id_wikidata'];
    } else {
        $id_wikidata = "";
    }
    
    
    if(isset($prenom_autrice) && !empty($prenom_autrice)){
        $autrice = new Autrice(array('id_autrice' => NULL, 'prenom_autrice' => $prenom_autrice, 'nom_autrice' => $nom_autrice, 'sexe' => $sexe, 'naissance' => $naissance, 'deces' => $deces, 'naissance_str' => $nstr, 'deces_str' => $dstr, 'minibio' => $biographie, 'image_autrice' => $image_autrice, 'image_autrice_source' => $image_source, 'id_bnf' => $id_bnf, 'id_isni' => $id_isni, 'id_wikidata' => $id_wikidata));
        
        $managerA->insertAutrice($autrice);
    }
    
    $id_recherche = $bdd->lastInsertId();
    
    if(!empty($id_aut)){
        $id_autrice = $id_aut;
        $autriceinsert = $managerA->getAutriceById($id_autrice);
        $nomautrice = $autriceinsert->fullName();
    } else if(isset($autrice)) {
        $id_autrice = $id_recherche;
        $autriceinsert = $managerA->getAutriceById($id_autrice);
        $nomautrice = $autriceinsert->fullName();
    } else {
        $id_autrice = "";
        $nom_autrice = "";
    }
    
    
    
    if(isset($_POST['nom_oeuvre'])){
        $new_oeuvre = strip_tags(htmlspecialchars($_POST['nom_oeuvre']));
    }
    
    if(isset($_POST['annee_oeuvre']) && !empty($_POST['annee_oeuvre'])){
        $annee_oeuvre = intval($_POST['annee_oeuvre']);
    } else {
        $annee_oeuvre = NULL;
    }
    
    if(isset($_POST['source_oeuvre'])){
        $source_oeuvre = strip_tags(htmlspecialchars($_POST['source_oeuvre']));
    } else {
        $source_oeuvre = "";
    }
    
    if(isset($_POST['achat_oeuvre'])){
        $achat_oeuvre = strip_tags(htmlspecialchars($_POST['achat_oeuvre']));
    } else {
        $achat_oeuvre = 0;
    }
    
    if(isset($new_oeuvre) && !empty($new_oeuvre)){
        $oeuvre = new Oeuvre(array('id_autrice_oeuvre' => $id_autrice, 'autrice_oeuvre' => $nomautrice, 'annee_oeuvre' => $annee_oeuvre, 'reference_oeuvre' => $new_oeuvre, 'source_oeuvre' => $source_oeuvre, 'achat_oeuvre' => $achat_oeuvre));
    
    $managerO->insertOeuvre($oeuvre);
        $idoeuvreinsert = $bdd->lastInsertId();
    }

    $date_creation = date('Y-m-d H:i:s');
    $date_modif = date('Y-m-d H:i:s');
    $ok = 0;
    
    
if(!empty($id_oeu)) {
    
    
    $extrait = new Extrait(array('titre_extrait' => $titre_extrait, 'texte_extrait' => $texte_extrait, 'intro_extrait' => $intro_extrait, 'droits_intro_extrait' => $droits_intro_extrait, 'source_extrait' => $source_extrait, 'utilisatrice_extrait' => $utilisatrice_extrait, 'oeuvre_extrait' => $id_oeu, 'date_creation_extrait' => $date_creation, 'date_modif_extrait' => $date_modif, 'ok_extrait' => $ok));
    
} else {
    $extrait = new Extrait(array('titre_extrait' => $titre_extrait, 'texte_extrait' => $texte_extrait, 'intro_extrait' => $intro_extrait, 'droits_intro_extrait' => $droits_intro_extrait, 'source_extrait' => $source_extrait, 'utilisatrice_extrait' => $utilisatrice_extrait, 'oeuvre_extrait' => $idoeuvreinsert, 'date_creation_extrait' => $date_creation, 'date_modif_extrait' => $date_modif, 'ok_extrait' => $ok));
}

    $liste_tags = explode(",", $_POST['id_tags']);
    
    $tags = array();
    
    for($j = 0; $j < sizeof($liste_tags); $j++){
        $tags[$j] = intval($liste_tags[$j]);
    }
    
    
    
    
    if(isset($_POST['theme_crea']) && !empty($_POST['theme_crea'])){
        $different_tags = explode(",", $_POST['theme_crea']);
        
        for($i = 0; $i < sizeof($different_tags); $i++){
            
            $chaine_tag = $managerT->formatTag($different_tags[$i]);
            
            if(!empty($chaine_tag)){
                $tag = new Tag(array('nom_tag' => $chaine_tag, 'type_tag' => 0));
            
            $managerT->insertTag($tag);
            
            $tagid = $bdd->lastInsertId();
            $index = sizeof($tags);
            $tags[$index] = $tagid;
            } else {
                continue;
            }
        }
    }
    
    if(isset($_POST['periode_crea']) && !empty($_POST['periode_crea'])){
        $different_periodes = explode(",", $_POST['periode_crea']);
        
        for($k = 0; $k < sizeof($different_periodes); $k++){
            $chaine_periode = $managerT->formatTag($different_periodes[$k]);
            
            if(!empty($chaine_periode)){
                $tag2 = new Tag(array('nom_tag' => $chaine_periode, 'type_tag' => 1));
            
            $managerT->insertTag($tag2);
            
            $periodeid = $bdd->lastInsertId();
            $index2 = sizeof($tags);
            $tags[$index2] = $periodeid;
            } else {
                continue;
            }
        }
    }
    
    if(isset($_POST['genre_crea']) && !empty($_POST['genre_crea'])){
        $different_genres = explode(",", $_POST['genre_crea']);
        
        for($l = 0; $l < sizeof($different_genres); $l++){
            $chaine_genre = $managerT->formatTag($different_genres[$l]);
            
            if(!empty($chaine_genre)){
                $tag3 = new Tag(array('nom_tag' => $chaine_genre, 'type_tag' => 2));
            
            $managerT->insertTag($tag3);
            
            $genreid = $bdd->lastInsertId();
            $index3 = sizeof($tags);
            $tags[$index3] = $genreid;
            } else {
                continue;
            }
        }
    }
    
    if(isset($_POST['notion_crea']) && !empty($_POST['notion_crea'])){
        $different_notions = explode(",", $_POST['notion_crea']);
        
        for($m = 0; $m < sizeof($different_notions); $m++){
            $chaine_notion = $managerT->formatTag($different_notions[$m]);
            
            if(!empty($chaine_notion)){
                $tag4 = new Tag(array('nom_tag' => $chaine_notion, 'type_tag' => 3));
            
            $managerT->insertTag($tag4);
            
            $notionid = $bdd->lastInsertId();
            $index4 = sizeof($tags);
            $tags[$index4] = $notionid;
            } else {
                continue;
            }
        }
    }
    
 

//$extrait = strip_tags($extrait,"<p><b><i><u><h1><h2><h3><h4><h5>");


/*
$sql = 'SELECT * FROM 2etexte_utilisatrice,2etexte_extrait,2etexte_contient_extraits,2etexte_oeuvre,2etexte_autrice WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait=2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice AND 2etexte_extrait.id_extrait='.$id_extrait;
$req = mysqli_query($link, $sql)
or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
$data = mysqli_fetch_assoc($req);
*/
?>

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Ajout d'un extrait</h1>
  <hr/>
   <?php
      $managerE->insertExtrait($extrait, $tags, $bdd);
      
      echo "<p>Extrait ajouté</p>";
      echo "<a href=\"extrait_ajout.php\">Retour</a>";
      ?>
  </div>
</div>
    </div>
    </div>

  
<?php
include("./footer.php");
?> 
</body>
</html>
