<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Profil public</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />
</head>
<body style="background-color:white;font-size:12pt;">
    <SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
    include('header.php');
    include('parameters.php');
    include('functions.php');
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    
    ?>
    <div style="background-color:#F5F5F5;margin-top:20px;padding:20px;">
        <div class="container">
            <div class="panel panel-default" style="text-align:center;padding:20px;">
                <div class="panel-body">
                    <?php
                    $id = intval($_GET['id']);
                    $manager = new UtilisatriceManager($bdd);
                    
                    $utilisatrice = $manager->getUtilisatriceById($id);
                    
                    ?>
                    <h1>Profil de <?php  echo $utilisatrice->getPrenom_utilisatrice(); ?> <?php echo $utilisatrice->getNom_utilisatrice(); ?></h1>
                    <img src="<?php echo $utilisatrice->getPhoto_utilisatrice(); ?>" alt="Photo de <?php echo $utilisatrice->getPrenom_utilisatrice(); ?> <?php echo $utilisatrice->getNom_utilisatrice(); ?>" title="Photo de <?php echo $utilisatrice->getPrenom_utilisatrice(); ?> <?php echo $utilisatrice->getNom_utilisatrice(); ?>" class="roundedImage"><br><br><br><br>
                    <a href="mailto:<?php echo $utilisatrice->getMail_utilisatrice(); ?>"><?php echo $utilisatrice->getMail_utilisatrice(); ?></a><br><br>
                    <?php 
                    if(!empty($utilisatrice->getPresentation_utilisatrice())){
                        ?>
                        <p><?php echo $utilisatrice->getPresentation_utilisatrice(); ?></p>
                        <?php
                    }
                    
                    if(!empty($utilisatrice->getLien_utilisatrice2())){
                        ?>
                        <a href="<?php echo $utilisatrice->getLien_utilisatrice2(); ?>">Lien vers le site de <?php  echo $utilisatrice->getPrenom_utilisatrice(); ?> <?php echo $utilisatrice->getNom_utilisatrice(); ?></a><br><br>
                        <?php
                    }
                    
                    if(!empty($utilisatrice->getAcademie())){
                        ?>
                        <p>Académie de <?php echo $utilisatrice->getAcademie(); ?></p>
                        <?php
                    }
                    ?>
                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>">Retour</a>
                </div>
            </div>
        </div>
    </div>
    <?php
    include('footer.php');
    ?>
</body>
</html>