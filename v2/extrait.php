<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("parameters.php");
include("functions.php");
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    

$id_extrait = intval($_GET['id']);
    $manager = new ExtraitManager($bdd);
    $extrait = $manager->getExtraitDonneesById($id_extrait);
    $tags = $manager->getTagsByExtrait($id_extrait);
    $utilisatrice = $manager->getUtilisatriceOfExtrait($id_extrait);

?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading"><?php echo $extrait["titre_extrait"]; ?></h1>
  <hr/>
<?php
   $manager->displayText($id_extrait,"h2");
?>
<p>Tags: </p>
<ul id="liste_tags">
    <?php
    echo $tags;
    ?>
</ul><br>
 <a href="extraits.php">Retour</a>
  </div>
</div>
   <?php
    if($_SESSION !== array()){
            ?>
            <fieldset><legend>Ajoutez d'autres tags si vous le souhaitez:</legend>
                <input type="hidden" name="idextrait" id="idextrait" value="<?php echo $id_extrait; ?>">
            <select name="selectionfiltre" id="selectionfiltre" class="form-control" required>
               <option value="">-- Choisissez un type de tag --</option>
                <option value="0">Thème</option>
                <option value="1">Période</option>
                <option value="2">Genre littéraire</option>
                <option value="3">Notion</option>
            </select><br><br>
            <div id="dynacloud" class="dynacloud"></div>
            <input type="text" name="tagsselect" id="tagsselect" required class="form-control" readonly><br><br>
            <input type="button" id="enlevetag" value="Enlever le dernier tag"><br><br>
            <input type="hidden" name="id_tags" id="id_tags" required>
            <label for="validecreation">Je veux créer de nouveaux tags&nbsp;</label><input type="checkbox" name="validecreation" id="validecreation" value="creation"><br><br>
            <div id="creationtag"></div>
            <input type="submit" value="Ajouter les nouveaux tags" id="envoi" class="btn btn-default">
            <div id="resultats"></div>
            </fieldset>
            <?php
    } else {
        echo "<p>Vous pouvez contribuer en rajoutant des tags. Pour cela, connectez-vous.</p>";
    }
    ?>
    </div>
    </div>
  
<?php
include("footer.php");
?>
<script src="js/jquery-3.2.1.min.js"></script>
<script>
    
    var checkboxtag = document.getElementById('validecreation'),
        creationtag = document.getElementById('creationtag');
    
    if(checkboxtag){
        checkboxtag.addEventListener('click', function(){
           if(checkboxtag.checked){
               creationtag.innerHTML = `<p>Créez de nouveaux tags (si vous voulez mettre plusieurs tags, mettre des virgules pour séparer les différents tags)</p>
<label for="theme_crea">Thème(s)</label><input type="text" name="theme_crea" id="theme_crea" class="form-control"><br><br>

<label for="periode_crea">Période(s)</label><input type="text" name="periode_crea" id="periode_crea" class="form-control"><br><br>

<label for="genre_crea">Genre(s) littéraire(s)</label><input type="text" name="genre_crea" id="genre_crea" class="form-control"><br><br>

<label for="notion_crea">Notion(s)</label><input type="text" name="notion_crea" id="notion_crea" class="form-control"><br><br>`;
           } else {
               creationtag.innerHTML = "";
           }
        });
    }
    
    function verifMot(tableau, mot){
                                   
                                   var flag = false;
                                   
                                   while(!flag){
                                       for(var i = 0; i < tableau.length; i++){
                                       if(tableau[i] == mot){
                                           flag = true;
                                           break;
                                       }
                                   }
                                       break;
                                   }
                                   
                                   return flag;
                               }
    
   
    
    function enleveLeTag(tableauVerif){
            
            var valeur = "";
            var nouveauTableau = tableauVerif.slice(0, tableauVerif.length - 1);
            
            if(nouveauTableau !== []){
                valeur = nouveauTableau.join(",");
            }
            return valeur;
            
        }
        
        function enleveLid(tableauId){
            
            var valeur = "";
            var nouveauTableau = tableauId.slice(0, tableauId.length - 1);
            
            if(nouveauTableau !== []){
                valeur = nouveauTableau.join(',');
            }
            return valeur;
        }
    
    
    
    $(document).ready(function(){
        
        $('#selectionfiltre').change(function(){
            $.ajax({
               url: 'test/ajax/liste_tags.php',
                method: 'GET',
                data: {t: $('#selectionfiltre').val()},
                success: function(code, statut){
                    
                    if(code){
                        if($('#dynacloud').html()){
                        $('#dynacloud').html('');
                        $(code).appendTo('#dynacloud');
                            
                    } else {
                        $(code).appendTo('#dynacloud');
                    }
                    
                    if($('#dynacloud span')){
                        
                        
                       
                        $('#dynacloud span').click(function(){
                            
                           if($('#tagsselect').val() !== ""){
                               var compteVirgule = 0;
                               
                               var tableauVirgule = $('#tagsselect').val().split(',');
                               
                               var flag = verifMot(tableauVirgule, $(this).text());
                               
                               
                               if(!flag){
                                   $('#tagsselect').val($('#tagsselect').val() + "," + $(this).text());
                                   
                                   if($('#id_tags').val()){
                                       $('#id_tags').val($('#id_tags').val() + "," + $(this).attr('tabindex'));
                                   } else {
                                       $('#id_tags').val($(this).attr('tabindex'));
                                   }
                                   
                               }
                               
                           } else {
                               $('#tagsselect').val($(this).text());
                               
                               $('#id_tags').val($(this).attr('tabindex'));
                               
                           }
                            
                            if($('#tagsselect').val() == ""){
                                $('#envoi').attr('disabled', true);
                            } else {
                                $('#envoi').attr('disabled', false);
                            }
                            
                        });
                    }
                    
                    
                } else {
                    $('#dynacloud').html('');
                    $('#tagsselect').val('');
                }
        } 
        });
        });
        
        $('#enlevetag').click(function(){
                            
                            var tableauVirgule = $('#tagsselect').val().split(',');
                var tableauId = $('#id_tags').val().split(',');
                            
                                  $('#tagsselect').val(enleveLeTag(tableauVirgule));
                $('#id_tags').val(enleveLid(tableauId));
                
                
                if($('#tagsselect').val() == ""){
                                $('#envoi').attr('disabled', true);
                            } else {
                                $('#envoi').attr('disabled', false);
                            }
                               });
        
        
        
        $('#envoi').click(function(){
            
            var verifSelect = $('#tagsselect').val().trim();
            
            if(verifSelect == ""){
                $('#resultats').html("");
                $('#resultats').html("<p>Vous n'avez pas choisi de tags parmi ceux proposés.</p>");
            } else if(document.getElementById('validecreation').checked){
                
                var verif = ($('#theme_crea').val().trim().replace(",", "") == "" && $('#periode_crea').val().trim().replace(",", "") == "" && $('#genre_crea').val().trim().replace(",", "") == "" && $('#notion_crea').val().trim().replace(",", "") == "");
                
                if(verif){
                    $('#resultats').html("");
                $('#resultats').html("<p>Vous n'avez rien mis comme nom de tags</p>");
                } else {
                    $.ajax({
               url: 'rajout_tag.php',
                method: 'POST',
                data: {idextrait: parseInt($('#idextrait').val().trim()), idtags: $('#id_tags').val().trim(), theme_crea: ($('#theme_crea') ? $('#theme_crea').val() : ""), periode_crea: ($('#periode_crea') ? $('#periode_crea').val() : ""), genre_crea: ($('#genre_crea') ? $('#genre_crea').val() : ""), notion_crea: ($('#notion_crea') ? $('#notion_crea').val() : "")},
                success: function(code, statut){
                    $('#resultats').html("");
                    window.location.reload();
                },
                error: function(jqXhr, statut, data){
                    $('#resultats').html("");
                    $(data).appendTo('#resultats');
                }
            });
                }
            } else {
                $.ajax({
               url: 'rajout_tag.php',
                method: 'POST',
                data: {idextrait: parseInt($('#idextrait').val().trim()), idtags: $('#id_tags').val().trim(), theme_crea: ($('#theme_crea') ? $('#theme_crea').val() : ""), periode_crea: ($('#periode_crea') ? $('#periode_crea').val() : ""), genre_crea: ($('#genre_crea') ? $('#genre_crea').val() : ""), notion_crea: ($('#notion_crea') ? $('#notion_crea').val() : "")},
                success: function(code, statut){
                    $('#resultats').html("");
                    window.location.reload();
                },
                error: function(jqXhr, statut, data){
                    $('#resultats').html("");
                    $(data).appendTo('#resultats');
                }
            });
            }
    });
    });
    
    </script>
</body>
</html>
