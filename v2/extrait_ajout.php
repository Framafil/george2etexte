<?php
session_start();
header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="style.css" />
   <link href="dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="images/icone_george2etexte.ico" type="images/x-icon" />
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<!--<script src="./js/ckeditor/ckeditor.js"></script>-->
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("parameters.php");
include("functions.php");
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
 <a href="index.php">Retour</a>
  <div class="panel-body">
  <h1 class="form-signin-heading">Ajout d'un extrait</h1>
  <hr>
  <form action="extrait_ajout_ok.php" method="post" enctype="multipart/form-data">
  
  <br><br>
  
          <label for="autrice">Autrice:</label><br><input type="text" name="autrice" id="autrice" required class="form-control requis"><br><br>
  <input type="hidden" name="id_autrice" id="id_autrice" required value="-1">
  <input type="hidden" name="id_bnf" id="id_bnf">
  <input type="hidden" name="id_isni" id="id_isni">
  <input type="hidden" name="id_wikidata" id="id_wikidata">
  
  
  
  <div id="listeautrices"></div>
  
  <br><br>
    
    <label for="oeuvre">&OElig;uvre d'où est tiré l'extrait:</label><br><input type="text" name="oeuvre" id="oeuvre" required class="form-control requis"><br><br>
    <input type="hidden" name="id_oeuvre" id="id_oeuvre" required>
    
    <div id="contenu" style="display: none;"></div>
    <div id="listeoeuvres"></div>
    
    <br><br>
     
      
  <label for="titre_extrait">Titre de l'extrait :</label><br>
  <input type="text" name="titre_extrait" id="titre_extrait" required class="form-control">
  <br/><br/>
      <p>Introduction (facultative), courte présentation de l'extrait, destinée aux enseignantes et enseignants qui l'utiliseront :</p><br>
  <textarea name="intro_extrait" id="intro_extrait"></textarea><br>
      <p>Sous quelle licence de diffusion souhaitez-vous mettre à disposition le texte d'introduction ci-dessus ?</p> 
  <select name="droits_intro_extrait" id="droits_intro_extrait" class="form-control" required>
  <option value="by-sa" selected>Creative Commons CC BY-SA 4.0 (attribution, partage dans les mêmes conditions)</option>
  <option value="by-nc-sa">Creative Commons CC BY-NC-SA 4.0 (attribution, partage dans les mêmes conditions, pas d'utilisation commerciale)</option>
  </select>
  <br><br>
      <p>Extrait :</p><br>
  <textarea name="texte_extrait" id="texte_extrait" required></textarea>
  <br>
  <label for="source_extrait">Source de l'extrait (lien éventuel vers une page web où se trouve cet extrait) :</label><br>
  <input type="url" name="source_extrait" id="source_extrait" class="form-control">
  <br><br>
  <p>Type de tag:</p>
<select name="selectionfiltre" id="selectionfiltre" required class="form-control">
       <option value="">-- Choisissez un type de tag --</option>
        <option value="0">Thème</option>
        <option value="1">Période</option>
        <option value="2">Genre littéraire</option>
        <option value="3">Notion</option>
        </select><br><br>
        <div id="dynacloud" class="dynacloud"></div>
        <p>Choisissez au moins un tag parmi la liste proposée</p>
        <input type="text" name="tagsselect" id="tagsselect" required class="form-control" readonly><br><br>
        <input type="button" id="enlevetag" value="Enlever le dernier tag"><br><br>
        <input type="hidden" name="id_tags" id="id_tags" required>
  <label for="validecreation">Je veux créer un tag</label><input type="checkbox" name="validecreation" id="validecreation" value="creation"><br><br>
  <div id="creationtag"></div>
  <input type="submit" value="Ajouter" id="envoi" class="btn btn-default">
  </form>
  </div>
</div>
    </div>
    </div>

  
<?php
include("./footer.php");
?> 
<script>
  CKEDITOR.replace( 'intro_extrait' );
  CKEDITOR.replace( 'texte_extrait' );
  </script>
  <script>
      
      function toutEstCliquable(div, input){
          var collectP = document.querySelectorAll('#' + div + ' .titre_oeuvre');
          
          var collectPhide = document.querySelectorAll('#' + div + ' .auteur_oeuvre');
          
          var collectPid = document.querySelectorAll('#' + div + ' .idoeuvre');
          
          var collectPaut = document.querySelectorAll('#' + div + ' .idaut');
          
          var listeautrices = document.getElementById(div);
          var autrice = document.getElementById(input);
          var hidden = document.getElementById('id_oeuvre');
          var hiddenA = document.getElementById('id_autrice');
          
          var realAutrice = document.getElementById('autrice');
          
          for(var i = 0; i < collectP.length; i++){
              collectP[i].addEventListener('click', function(e){
                  
                  autrice.value = e.target.textContent.trim();
                  
                  if(input == 'oeuvre'){
                      
                      for(var j = 0; j < collectPhide.length; j++){
                          
                          if(collectP[j] == e.target){
                              realAutrice.value = "";
                              realAutrice.value = collectPhide[j].textContent.trim();
                              e.preventDefault();
                              break;
                          }
                          
                      }
                      
                      for(var k = 0; k < collectPid.length; k++){
                          if(collectP[k] == e.target){
                              hidden.value = "";
                              hidden.value = collectPid[k].textContent;
                              e.preventDefault();
                              break;
                          }
                      }
                      
                      for(var l = 0; l < collectPaut.length; l++){
                          if(collectP[l] == e.target){
                              hiddenA.value = "";
                              hiddenA.value = collectPaut[l].textContent;
                              e.preventDefault();
                              break;
                          }
                      }
                  }
                  
                  listeautrices.innerHTML = "";
                  bloqueEnvoi('listeautrices', 'creation_auteur');
              });
          }
      }
      
      function toutEstCliquable2(div, input){
          var collectP = document.querySelectorAll('#' + div + ' .nom_autrice');
          
          var collectPid = document.querySelectorAll('#' + div + ' .idautrice');
          
          
          var listeautrices = document.getElementById(div);
          var autrice = document.getElementById(input);
          var hidden = document.getElementById('id_autrice');
          
          
          for(var i = 0; i < collectP.length; i++){
              collectP[i].addEventListener('click', function(e){
                  autrice.value = this.textContent.trim();
                  
                  for(var j = 0; j < collectPid.length; j++){
                      if(collectP[j] == e.target){
                          hidden.value = "";
                          hidden.value = collectPid[j].textContent;
                          e.preventDefault();
                          break;
                      }
                  }
                  
                  listeautrices.innerHTML = "";
                  bloqueEnvoi('listeautrices', 'creation_auteur');
              });
          }
      }
      
      function toutEstCliquableJson(div, input){
          var prenomPlusNom, naissance, mort, jsonautrice;
          
          var collectPJson = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices .json_autrices');
          
          var collectBnf = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices .idbnf');
          
          var collectIsni = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices .idisni');
          
          var collectWikidata = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices .idwikidata');
          
          var collectNaissance = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices #annee_naissance');
          
          var collectMort = document.querySelectorAll('#' + div + ' #choixauteur .clique_autrices #annee_mort');
          
          var listeautrices = document.getElementById(div);
          var autrice = document.getElementById(input);
          var idbnf = document.getElementById('id_bnf');
          var idisni = document.getElementById('id_isni');
          var idwikidata = document.getElementById('id_wikidata');
          
          for(var i = 0; i < collectPJson.length; i++){
              
              collectPJson[i].addEventListener('click', function(e){
              autrice.value = "";
              autrice.disabled = true;
                  
                  for(var j = 0; j < collectPJson.length; j++){
                      if(collectPJson[j] == e.target){
                          idbnf.value = collectBnf[j].textContent;
                      idisni.value = collectIsni[j].textContent;
                      idwikidata.value = collectWikidata[j].textContent;
                      naissance = collectNaissance[j].textContent;
                      mort = collectMort[j].textContent;
                          jsonautrice = collectPJson[j].textContent;
                          break;
                      }
                  }
              
              listeautrices.innerHTML = "<br><br><input type=\"button\" id=\"creer_auteur\" value=\"Ne pas créer une autrice\"><br><br>";
              
              if(jsonautrice.indexOf(" ") !== -1){
                  prenomPlusNom = jsonautrice.split(" ");
                  listeautrices.innerHTML += `<div id="creation_auteur"><label for="prenom_autrice">Prénom de l'autrice</label><input type="text" name="prenom_autrice" id="prenom_autrice" required class="form-control" value=`+ prenomPlusNom[0] +`><br><br>
<label for="nom_autrice">Nom de l'autrice</label><input type="text" name="nom_autrice" id="nom_autrice" required class="form-control" value=`+ prenomPlusNom[1] + (prenomPlusNom[2] ? "&nbsp;" + prenomPlusNom[2] : "") +`><br><br><p>Sexe: </p>
<label for="masculin">Masculin</label><input type="radio" name="sexe_autrice" id="masculin" value="0" required>

<label for="feminin">Féminin</label><input type="radio" name="sexe_autrice" id="feminin" value="1" required><br><br>

<label for="naissance">Année de naissance</label><input type="number" name="naissance" id="naissance" required class="form-control" value="`+ naissance +`"><br>

<label for="nstr">Pas sûr(e) de la date ?</label><input type="checkbox" name="nstr" id="nstr" value="1"><br><br>

<label for="deces">Année de décès (falcultatif)</label><input type="number" name="deces" id="deces" class="form-control" value="`+ mort +`"><br>

<label for="dstr">Pas sûr(e) de la date ?</label><input type="checkbox" name="dstr" id="dstr" value="1"><br><br>

<label for="biographie">Biographie de l'auteur (falcultatif): </label><textarea name="biographie" id="biographie" class="form-control" cols="30" rows="10" placeholder="Biographie..."></textarea><br><br>

<p>Choisissez une photo de l'auteur: (falcultatif)</p>
<input type="file" name="photo" id="photo" accept="image/jpeg"><br><br>

<label for="lien_image">Adresse source de la photo (falcultatif)</label><input type="url" name="lien_image" id="lien_image" class="form-control"></div>`;
              } else {
                  listeautrices.innerHTML += `<div id="creation_auteur"><label for="prenom_autrice">Prénom de l'autrice</label><input type="text" name="prenom_autrice" id="prenom_autrice" required class="form-control" value=`+ jsonautrice +`><br><br>
<label for="nom_autrice">Nom de l'autrice</label><input type="text" name="nom_autrice" id="nom_autrice" required class="form-control"><br><br><p>Sexe: </p>
<label for="masculin">Masculin</label><input type="radio" name="sexe_autrice" id="masculin" value="0" required>

<label for="feminin">Féminin</label><input type="radio" name="sexe_autrice" id="feminin" value="1" required><br><br>

<label for="naissance">Année de naissance</label><input type="number" name="naissance" id="naissance" required class="form-control" value="`+ naissance +`"><br>

<label for="nstr">Pas sûr(e) de la date ?</label><input type="checkbox" name="nstr" id="nstr" value="1"><br><br>

<label for="deces">Année de décès (falcultatif)</label><input type="number" name="deces" id="deces" class="form-control" value="`+ mort +`"><br>

<label for="dstr">Pas sûr(e) de la date ?</label><input type="checkbox" name="dstr" id="dstr" value="1"><br><br>

<label for="biographie">Biographie de l'auteur (falcultatif): </label><textarea name="biographie" id="biographie" class="form-control" cols="30" rows="10" placeholder="Biographie..."></textarea><br><br>

<p>Choisissez une photo de l'auteur: (falcultatif)</p>
<input type="file" name="photo" id="photo" accept="image/jpeg"><br><br>

<label for="lien_image">Adresse source de la photo (falcultatif)</label><input type="url" name="lien_image" id="lien_image" class="form-control"></div>`;
              }
                  bloqueEnvoi('listeautrices', 'creation_auteur');
              clickButton();
              
          });
              
          }
          
          
              }
      
      function clickButton(){
          var clickB = document.getElementById('creer_auteur');
          var autrice = document.getElementById('autrice');
          var listesautrices = document.getElementById('listeautrices');
          var hidden = document.getElementById('id_autrice');
          var idbnf = document.getElementById('id_bnf');
          var idisni = document.getElementById('id_isni');
          var idwikidata = document.getElementById('id_wikidata');
          var id_autrice = document.getElementById('id_autrice');
          
          clickB.addEventListener('click', function(){
              
              if(clickB.value === "Créer une autrice"){
                  autrice.value = "";
          autrice.required = false;
              autrice.disabled = true;
                  hidden.value = "";
                  hidden.required = false;
              clickB.value = "Ne pas créer d'autrice";
                  
                  $.ajax({
                     url: 'test/ajax/options_auteur.php',
                      method: 'GET',
                      data: {a: -1},
                      success: function(code, statut){
                          $(code).appendTo('#creation_auteur');
                          bloqueEnvoi('listeautrices', 'creation_auteur');
                      }
                  });
                  
              } else {
                  autrice.value = "";
          autrice.required = true;
              autrice.disabled = false;
                  hidden.value = "";
                  hidden.required = true;
                  idbnf.value = "";
                  idisni.value = "";
                  idwikidata.value = "";
                  id_autrice.value = "-1";
                  listeautrices.innerHTML = "";
                   bloqueEnvoi('listeautrices', 'creation_auteur');
              }
          });
      }
      
      function bloqueEnvoi(div, div2){
          var envoi = document.getElementById('envoi');
          var listeautrices = document.getElementById(div);
          var tagsselect = document.getElementById('tagsselect');
          
          if(document.getElementById(div2)){
              var creationAuteur = document.getElementById(div2);
          }
          
          if(listeautrices.innerHTML !== "" && !creationAuteur){
              envoi.disabled = true;
          } else if(tagsselect.value == "") {
              envoi.disabled = true;
          } else {
              envoi.disabled = false;
          }
          
      }
      
      function recupIdAutrice(){
          var tableauResults = $('#idautrice');
          
          for(var i = 0; i < tableauResults.length; i++){
              tableauResults[i].click(function(e){
                 for(var j = 0; j < tableauId.length; j++){
                  if(tableauResults[j] == e.target){
                      $('#id_autrice').val(tableauId[j].text());
                      break;
                  }
              } 
              });
          }
      }
      
      function donneesWikidata(autrice){
          var idBnf = [], idIsni = [], idWikidata = [], texteARenvoyer, autriceFormat, naissance = [], mort = [], travail = [];
          
          texteARenvoyer = "<div><strong>En cherchant sur Wikidata, nous avons trouvé:</strong></div>";
          
          /*
          
          //Formatage des noms d'autrices (majuscule + gestion des espaces)
          
          if(autrice.indexOf(" ") !== -1){
              autriceFormat = autrice.split(" ");
          } else {
              autriceFormat = autrice;
          }
          
          
          if(typeof(autriceFormat) === 'object'){
              for(var i = 0; i < autriceFormat.length; i++){
              if(autriceFormat[i] !== ""){
                  autriceFormat[i] = autriceFormat[i].charAt(0).toUpperCase() + autriceFormat[i].substring(1).toLowerCase();
              }
          }
          
          var autriceFinal, autriceMontrer;
          
          if(autriceFormat.length > 1){
              autriceFinal = autriceFormat.join("%20");
              autriceMontrer = autriceFormat.join(" ");
          } else {
              autriceFinal = autriceFormat[0];
              autriceMontrer = autriceFormat[0];
          }
          } else {
              autriceFinal = autriceFormat.charAt(0).toUpperCase() + autriceFormat.substring(1).toLowerCase();
              autriceMontrer = autriceFormat.charAt(0).toUpperCase() + autriceFormat.substring(1).toLowerCase();
          }*/
          
          var autriceFinal = autrice, autriceMontrer = autrice;
          
          var requeteSparql = 'https://query.wikidata.org/sparql?query=SELECT%20distinct%20%3Fautrice%20%3Ftravail%20%3FbnfId%20%3Fisni%20%3FwikiData%20%3Fnaissance%20%28YEAR%28%3Fnaissance%29%20as%20%3FanneeNaissance%29%20%28YEAR%28%3Fmort%29%20as%20%3FanneeMort%29%20%3Fmort%0AWHERE%20%7B%20%0A%20%20%7B%3FwikiData%20rdfs%3Alabel%20%22'+ encodeURI(autriceFinal) +'%22%40fr.%7D%0A%20%20%7B%20%3FwikiData%20wdt%3AP106%20%3Fwork%20.%7D%0A%20%20%7B%20%3Fwork%20rdfs%3Alabel%20%3Ftravail%20filter%20%28lang%28%3Ftravail%29%20%3D%20%22fr%22%29.%20%7D%0A%20%20OPTIONAL%20%7B%20%3FwikiData%20wdt%3AP268%20%3FbnfId%20.%7D%20%0A%20%20OPTIONAL%20%7B%20%3FwikiData%20wdt%3AP213%20%3Fisni%20.%7D%20%0A%20%20OPTIONAL%20%7B%3FwikiData%20wdt%3AP569%20%3Fnaissance.%20%7D%20%0A%20%20OPTIONAL%20%7B%3FwikiData%20wdt%3AP570%20%3Fmort.%20%7D%20%0A%20%20%3FwikiData%20rdfs%3Alabel%20%3Fautrice%20filter%20%28lang%28%3Fautrice%29%20%3D%20%22fr%22%29.%20%0A%7D%20%0AGROUP%20BY%20%3FwikiData%20%3Ftravail%20%3Fautrice%20%3FbnfId%20%3Fisni%20%3Fnaissance%20%3Fmort%20ORDER%20BY%20ASC%28%3Fnaissance%29&format=json';
            
            $.getJSON(requeteSparql, function(data){
                
                if(data.results.bindings.length !== 0){
                    
                    for(var j = 0; j < data.results.bindings.length; j++){
                        if(data.results.bindings[j].bnfId){
                        idBnf[j] = data.results.bindings[j].bnfId.value;
                    } else {
                        idBnf[j] = "";
                    }
                    
                    if(data.results.bindings[j].isni){
                        idIsni[j] = data.results.bindings[j].isni.value;
                    } else {
                        idIsni[j] = "";
                    }
                    
                    if(data.results.bindings[j].wikiData){
                        idWikidata[j] = data.results.bindings[j].wikiData.value;
                    } else {
                        idWikidata[j] = "";
                    }
                    
                    if(data.results.bindings[j].anneeNaissance){
                        naissance[j] = data.results.bindings[j].anneeNaissance.value;
                    } else {
                        naissance[j] = "";
                    }
                    
                    if(data.results.bindings[j].anneeMort){
                        mort[j] = data.results.bindings[j].anneeMort.value;
                    } else {
                        mort[j] = "";
                    }
                    
                    if(data.results.bindings[j].travail){
                        travail[j] = data.results.bindings[j].travail.value;
                    } else {
                        travail[j] = "";
                    }
                        
                        var choixAuteur = document.createElement('div');
                        choixAuteur.id = "choixauteur";
                        
                        if(naissance[j] == "" && mort[j] == ""){
                            
                            texteARenvoyer += '<div class="clique_autrices"><p class="json_autrices">'+ autriceMontrer +'</p><br><span class="boulot"> '+ travail[j].charAt(0).toUpperCase() + travail[j].substring(1).toLowerCase() +'</span><span id="annee_naissance"></span><span id="annee_mort"></span><p style="display: none;" class="idbnf">'+ idBnf[j] + '</p><p style="display: none;" class="idisni">'+ idIsni[j] +'</p><p style="display: none;" class="idwikidata">'+ idWikidata[j] +'</p></div><hr>';
                            
                        } else {
                            texteARenvoyer += '<div class="clique_autrices"><p class="json_autrices">'+ autriceMontrer +'</p><br><span class="boulot"> '+ travail[j].charAt(0).toUpperCase() + travail[j].substring(1).toLowerCase() +'</span> (<span id="annee_naissance">'+ naissance[j] +'</span>-<span id="annee_mort">'+ mort[j] +'</span>)<p style="display: none;" class="idbnf">'+ idBnf[j] + '</p><p style="display: none;" class="idisni">'+ idIsni[j] +'</p><p style="display: none;" class="idwikidata">'+ idWikidata[j] +'</p></div><hr>';
                        }
                        
                    }
                    
                    choixAuteur.innerHTML = texteARenvoyer;
                    document.getElementById('listeautrices').appendChild(choixAuteur);
                    
                }
                
                if($('#listeautrices').html() === ""){
                    
                    var textePerdu = "<span>Désolé, votre recherche n'a pas abouti. En revanche, si vous tapez en entier le nom de l'auteur(trice), Wikidata peut vous trouver quelque chose. <br><br> Ou peut-être voulez-vous créer une autrice</span><br><br><input type=\"button\" id=\"creer_auteur\" value=\"Créer une autrice\"><br><br><div id=\"creation_auteur\"></div>";
                    
                    $('#listeautrices').html("");
                                $('#listeautrices').html(textePerdu);
                                clickButton();
                } else {
                    toutEstCliquableJson('listeautrices', 'autrice');
                }
               
            });
      }
      
    $(document).ready(function(){
        
        $('#autrice').keyup(function(e){
            $.ajax({
               url: 'test/ajax/liste_autrices.php',
                method: 'GET',
                data: {r: $('#autrice').val().trim()},
                success: function(code, statut){
                    
                    var autrice = $('#autrice').val().trim();
                    
                    if(!$('#listeoeuvres #creation_oeuvre').text()){
                        $('#listeoeuvres').html('');
                    }
                    
                    
                    
                    
                    if(code){
                        
                       if($('#listeautrices').text()){
                           $('#listeautrices').html("");
                           $(code).appendTo('#listeautrices');
                           
                           toutEstCliquable2('listeautrices', 'autrice');
                           recupIdAutrice();
                           bloqueEnvoi('listeautrices', 'creation_auteur');
                       } else {
                           $(code).appendTo('#listeautrices');
                           toutEstCliquable2('listeautrices', 'autrice');
                           recupIdAutrice();
                           bloqueEnvoi('listeautrices', 'creation_auteur');
                       } 
                        
                        
                    } else {
                        if($('#autrice').val()){
                            $('#listeautrices').html("");
                            
                            donneesWikidata($('#autrice').val().trim());
                            
                            $('#envoi').attr('disabled', true);
                            
                        } else {
                            
                            if(e.which == 20){
                                  e.preventDefault();
                              } else {
                                  $('#oeuvre').val('');
                              }
                            $('#listeautrices').html('');
                            bloqueEnvoi('listeautrices', 'creation_auteur');
                            
                            
                        $('#id_autrice').val('-1');
                            $('#id_oeuvre').val('');
                            
                        }
                    }
                }
            });
    });
        
        
    });
      
      function clickButton2(){
          var clickB = document.getElementById('ajout_oeuvre');
          var oeuvre = document.getElementById('oeuvre');
          var creationoeuvre = document.getElementById('creation_oeuvre');
          var listeoeuvres = document.getElementById('listeoeuvres');
          var hidden = document.getElementById('id_oeuvre');
          
          clickB.addEventListener('click', function(){
              if(clickB.value === "Créer une oeuvre"){
                  oeuvre.value = "";
          oeuvre.required = false;
              oeuvre.disabled = true;
                  hidden.value = "";
                  hidden.required = false;
              clickB.value = "Ne pas créer d'oeuvre";
                  
                  creationoeuvre.innerHTML = `<label for="nom_oeuvre">Nom de l'œuvre</label><input type="text" name="nom_oeuvre" id="nom_oeuvre" required class="form-control"><br><br><div id=\"xmloeuvres\"></div><br><br>
<label for="annee_oeuvre">Année de sortie de l'œuvre (falcultatif)</label><input type="number" name="annee_oeuvre" id="annee_oeuvre" class="form-control"><br><br>
<label for="source_oeuvre">Source de l'œuvre (falcultatif)</label><input type="url" name="source_oeuvre" id="source_oeuvre" class="form-control"><br><br>
<label for="achat_oeuvre">Lien pour acheter l'œuvre (falcultatif)</label><input type="url" name="achat_oeuvre" id="achat_oeuvre" class="form-control"><br><br>`;
                  bloqueEnvoi('listeoeuvres', 'creation_oeuvre');
                  
              } else {
                  oeuvre.value = "";
          oeuvre.required = true;
              oeuvre.disabled = false;
                  hidden.value = "";
                  hidden.required = true;
                  listeoeuvres.innerHTML = "";
                  bloqueEnvoi('listeoeuvres', 'creation_oeuvre');
              }
          });
      }
      
      $(document).ready(function(){
         
          var texteLoser = "<span>Désolé, l'oeuvre n'est pas écrit par l'auteur. Peut-être voulez-vous créer une oeuvre</span><br><br><input type=\"button\" id=\"ajout_oeuvre\" value=\"Créer une oeuvre\"><br><br><div id=\"creation_oeuvre\"></div>";
          
          var texteInfo = "<span>Désolé, l'oeuvre n'existe pas. Peut-être voulez-vous créer une oeuvre</span><br><br><input type=\"button\" id=\"ajout_oeuvre\" value=\"Créer une oeuvre\"><br><br><div id=\"creation_oeuvre\"></div>";
          
          
          $('#oeuvre').keyup(function(e){
              
              if($("#oeuvre").val().trim()){
                  $.ajax({
                 url: 'test/ajax/liste_oeuvres.php',
                  method: 'GET',
                  data: {a: $('#autrice').val().trim(), o: $('#oeuvre').val().trim(), id_a: $('#id_autrice').val().trim(), cherche_a: ($("#prenom_autrice").val() ? $('#prenom_autrice').val().trim() : "")},
                  success: function(code, statut){
                      
                      if(!$('#listeautrices #creation_auteur').text()){
                           $('#listeautrices').html('');
                      }
                      
                      console.log(code);
                      
                      
                      if(code){
                          
                          if($('#listeoeuvres').text()){
                              $('#listeoeuvres').html('');
                              
                              $(code).appendTo('#listeoeuvres');
                              toutEstCliquable('listeoeuvres', 'oeuvre');
                              proposeXMLOeuvres($('#oeuvre').val().trim(), $('#autrice').val().trim());
                              bloqueEnvoi('listeoeuvres', 'creation_oeuvre');
                              
                              
                          } else {
                              $(code).appendTo('#listeoeuvres');
                              toutEstCliquable('listeoeuvres', 'oeuvre');
                              proposeXMLOeuvres($('#oeuvre').val().trim(), $('#autrice').val().trim());
                              bloqueEnvoi('listeoeuvres', 'creation_oeuvre');
                          }
                      } else {
                          if($('#oeuvre').val().trim()){
                              $('#listeoeuvres').html('');
                             
                             if($('#autrice').val().trim()){
                                 $('#listeoeuvres').html(texteLoser);
                             } else if($('#creation_auteur').html()) {
                                 $('#listeoeuvres').html('<span>Pas d\'oeuvre à afficher</span><br><br><input type=\"button\" id=\"ajout_oeuvre\" value=\"Créer une oeuvre\"><br><br><div id=\"creation_oeuvre\"></div>');
                             } else {
                                 $('#listeoeuvres').html(texteInfo);
                             }
                              
                              $('#envoi').attr('disabled', true);
                              
                              clickButton2();
                          } else {
                              
                              if(e.which == 20){
                                  e.preventDefault();
                              } else {
                                  $('#autrice').val('');
                              }
                              $('#listeoeuvres').html('');
                              bloqueEnvoi('listeoeuvres', 'creation_oeuvre');
                              $('#id_oeuvre').val('');
                              $('#id_autrice').val('-1');
                          }
                      }
                  }
              });
              } else {
                  $('#listeoeuvres').html("");
              }
          });
          
      });
      
      $(document).ready(function(){
         $('#oeuvre').change(function(){
             if($('#oeuvre').val() == "" && $('#autrice').attr('disabled') == false){
                 $('#autrice').val('');
             }
         });
          
          $('#autrice').change(function(){
              if($('#autrice').val() == "" && $('#oeuvre').attr('disabled') == false){
                  $('#oeuvre').val('');
              } 
          });
          
      });
      
      function replaceAll(code, searchStr, replaceStr){
            while(code.indexOf(searchStr) > -1){
                code = code.replace(searchStr, replaceStr);
            }
            return code;
        }
      
      function jsoncallback(data){
          var codeur = replaceAll(data, "mxc:datafield", "mxcdatafield");
          codeur = replaceAll(data, "mxc:subfield", "mxcsubfield");
          return codeur;
      }
      
      function proposeXMLOeuvres(oeuvre, autrice){
          $(document).ready(function(){
            $.ajax({
               url: <?php echo "'".$api."'"; ?>,
                method: 'GET',
                data: {read_all: "bib.author%20all%20%22" + encodeURI(autrice) + "%22%20and%20bib.title%20all%20%22" + encodeURI(oeuvre) + "%22"},
                dataType: 'html',
                success: function(code, statut){
                    if(code){
                        var codeur = jsoncallback(code);
                        $('#contenu').html("");
                        $('#contenu').html(codeur);
                        
                        var nouveauTableau = [];
                        
                        if($('mxcdatafield[tag="200"]>mxcsubfield[code="a"]')[0]){
                            for(var i = 0; i < $('mxcdatafield[tag="200"]>mxcsubfield[code="a"]').length; i++){
                        
                        var texteTableau = $('mxcdatafield[tag="200"]>mxcsubfield[code="a"]')[i].textContent.replace('[', '').replace(']', '');
                        
                        if(nouveauTableau.indexOf(texteTableau) !== -1){
                            nouveauTableau[i] = "";
                        } else {
                            nouveauTableau[i] = texteTableau;
                        }
                        
                    }
                            
                            for(var j = 0; j < nouveauTableau.length; j++){
                                if(nouveauTableau[j] !== "" && $('#listeoeuvres p').text() !== nouveauTableau[j]){
                                    $('#listeoeuvres').html($('#listeoeuvres').html() + "<p>" + nouveauTableau[j] + "</p>");
                                }
                            }
                            
                        }
                        
                    }
                }
            });
             
      });
      }
    </script>
  <script>
      window.addEventListener('load', function(){
          var envoi = document.getElementById('envoi');
          var iframe =  document.querySelector('#cke_texte_extrait .cke_inner #cke_2_contents iframe');
          
          
          
          var fenetre_contenu = iframe.contentWindow;
          var cke_contenu = iframe.contentWindow.document;
          
          function estVide(stringAVerifier){
              var flag = false;
              var chaineverif = "";
              var chainefinale = "";
              var i = 0;
              while(i < stringAVerifier.length){
                  if(stringAVerifier.charAt(i) == "<"){
                      flag = true;
                      while(flag){
                          if(stringAVerifier.charAt(i) == ">"){
                      flag = false;
                              i++;
                  } else {
                      i++;
                  }
                      }
                  } else {
                     chaineverif += stringAVerifier.charAt(i); 
                      i++;
                  }
                  
              }
              chainefinale = chaineverif.replace('""', '');
              if(chainefinale.length == 0){
                  return true;
              } else {
                  return false;
              }
          }
          
          fenetre_contenu.addEventListener('blur', function(){
            
              var cke_texte_extrait = iframe.contentWindow.document.querySelectorAll("p");
              
              for(var i = 0; i < cke_texte_extrait.length; i++){
                  if(estVide(cke_texte_extrait[i].innerHTML) == true || document.getElementById('id_tags').value == ""){
                      envoi.disabled = true;
                  } else {
                      envoi.disabled = false;
                  }
              } 
          });
      });
    </script>
    <script>
        
        function verifMot(tableau, mot){
                                   
                                   var flag = false;
                                   
                                   while(!flag){
                                       for(var i = 0; i < tableau.length; i++){
                                       if(tableau[i] == mot){
                                           flag = true;
                                           break;
                                       }
                                   }
                                       break;
                                   }
                                   
                                   return flag;
                               }
        
        function enleveLeTag(tableauVerif){
            
            var valeur = "";
            var nouveauTableau = tableauVerif.slice(0, tableauVerif.length - 1);
            
            if(nouveauTableau !== []){
                valeur = nouveauTableau.join(",");
            }
            return valeur;
            
        }
        
        function enleveLid(tableauId){
            
            var valeur = "";
            var nouveauTableau = tableauId.slice(0, tableauId.length - 1);
            
            if(nouveauTableau !== []){
                valeur = nouveauTableau.join(',');
            }
            return valeur;
        }
        
        $(document).ready(function(){
           $('#selectionfiltre').change(function(){
            $.ajax({
               url: 'test/ajax/liste_tags.php',
                method: 'GET',
                data: {t: $('#selectionfiltre').val()},
                success: function(code, statut){
                    
                    if(code){
                        if($('#dynacloud').html()){
                        $('#dynacloud').html('');
                        $(code).appendTo('#dynacloud');
                            
                    } else {
                        $(code).appendTo('#dynacloud');
                    }
                    
                    if($('#dynacloud span')){
                        
                        
                       
                        $('#dynacloud span').click(function(){
                            
                           if($('#tagsselect').val() !== ""){
                               var compteVirgule = 0;
                               
                               var tableauVirgule = $('#tagsselect').val().split(',');
                               
                               var flag = verifMot(tableauVirgule, $(this).text());
                               
                               
                               if(!flag){
                                   $('#tagsselect').val($('#tagsselect').val() + "," + $(this).text());
                                   
                                   if($('#id_tags').val()){
                                       $('#id_tags').val($('#id_tags').val() + "," + $(this).attr('tabindex'));
                                   } else {
                                       $('#id_tags').val($(this).attr('tabindex'));
                                   }
                                   
                               }
                               
                           } else {
                               $('#tagsselect').val($(this).text());
                               
                               $('#id_tags').val($(this).attr('tabindex'));
                               
                           }
                            
                            if($('#tagsselect').val() == ""){
                                $('#envoi').attr('disabled', true);
                            } else {
                                $('#envoi').attr('disabled', false);
                            }
                            
                        });
                    }
                    
                    
                } else {
                    $('#dynacloud').html('');
                    $('#tagsselect').val('');
                }
        } 
        });
        });
            $('#enlevetag').click(function(){
                            
                            var tableauVirgule = $('#tagsselect').val().split(',');
                var tableauId = $('#id_tags').val().split(',');
                            
                                  $('#tagsselect').val(enleveLeTag(tableauVirgule));
                $('#id_tags').val(enleveLid(tableauId));
                
                
                if($('#tagsselect').val() == ""){
                                $('#envoi').attr('disabled', true);
                            } else {
                                $('#envoi').attr('disabled', false);
                            }
                               });
        });
    </script>
    <script>
    var checkboxtag = document.getElementById('validecreation'),
        creationtag = document.getElementById('creationtag');
        
        checkboxtag.addEventListener('click', function(){
           if(checkboxtag.checked){
               creationtag.innerHTML = `<p>Créez de nouveaux tags (si vous voulez mettre plusieurs tags, mettre des virgules pour séparer les différents tags)</p>
<label for="theme_crea">Thème(s)</label><input type="text" name="theme_crea" id="theme_crea" class="form-control"><br><br>

<label for="periode_crea">Période(s)</label><input type="text" name="periode_crea" id="periode_crea" class="form-control"><br><br>

<label for="genre_crea">Genre(s) littéraire(s)</label><input type="text" name="genre_crea" id="genre_crea" class="form-control"><br><br>

<label for="notion_crea">Notion(s)</label><input type="text" name="notion_crea" id="notion_crea" class="form-control"><br><br>

`;
           } else {
               creationtag.innerHTML = "";
           }
        });
        
    </script>
    <script>
    var tagsselect = document.getElementById('tagsselect');
        var boutonEnvoi = document.getElementById('envoi');
        
        if(tagsselect.value == ""){
                boutonEnvoi.disabled = true;
            }
        
        tagsselect.addEventListener('change', function(){
            if(tagsselect.value == ""){
                boutonEnvoi.disabled = true;
            } else {
                boutonEnvoi.disabled = false;
            }
        });
    </script>
</body>
</html>
