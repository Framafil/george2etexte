<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="stylesheet" href="style.css">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />  
   <meta property="og:type" content="Plate-forme">
   <meta property="og:title" content="Le Deuxième Texte">
   <meta property="og:description" content="Venez découvrir les extraits d'autrices et d'auteurs">
   <meta property="og:image" content="http://theosenilh.fr/george2etexte/george2etexte/v2/images/logo_deuxieme_texte.JPG">
</head>

<body style="background-color:white;font-size:12pt;">

<!-- HEADER -->
<?php
include("header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">

<!-- PRESENTATION -->
<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body" style="font-family:Calibri;">
  <p>
  <i>George - Le deuxième texte</i> est une plateforme web,
  conçue lors du <a href="https://forum.etalab.gouv.fr/t/a-propos-de-la-categorie-hackegalitefh/3445">#HackEgalitéFH</a>,
  qui met à disposition des professeur·e·s (et du grand public) une base de textes écrits tant par des femmes que par des hommes,
  de la manière la plus exhaustive et paritaire possible.
  L’objectif est de donner plus de visibilité aux autrices dans les programmes scolaires,
  afin que les jeunes puissent s’identifier à des figures fortes, sans distinction de genre.
  </p>
  
  <p>
  <b><i>George - Le deuxième texte</i> est en cours de construction.</i></b> Suivez les avancées du projet <a href="http://george2etexte.wordpress.com">sur notre blog</a>.
  </p>
  
  <p>Retrouvez <a href="https://twitter.com/Georgele2etexte">notre fil Twitter</a>,
  <a href="https://www.facebook.com/George-le-deuxi%C3%A8me-texte-994051650727500/">notre page Facebook</a>,
  <a href="./pdf/PREZ_George_2eme texte.pdf">notre présentation</a> et 
  les maquettes de la plateforme
  (<a href="./images/maquette_accueil.png">page d'accueil</a>, <a href="./images/maquette_article.png">page d'article</a>).
  </p>

  
  </div>
</div>

<!-- RECHERCHE AUTRICES CONTEMPORAINES -->
<div style="font-size:20pt;text-align:center" class="titresPage">Je cherche une autrice contemporaine de...</div>
<div class="panel panel-default" style="text-align:center;padding:20px;margin-top:-10px;">
  <div class="panel-body" style="font-family:Calibri;">
  <!--<div style="text-align:center;background-color:blue;width:400px;">-->
  <span class="col-xs-10"><input type="text" id="nomAuteur" class="form-control" value="Pierre de Ronsard"></span>
  <button type="button" class="btn btn-primary" id="alterEgo" style="background-color:#8282D2">Rechercher</button>
  <!--</div>-->
  <div id="resultatsAlterEgo"></div>
  </div>
</div>



<!-- RECHERCHE PAR SIECLE ET NOTION -->
<!--
<div style="font-size:20pt;text-align:center" class="titresPage">Je cherche un texte de femme</div>
<div class="panel panel-default" style="text-align:center;padding:20px;margin-top:-10px;">
  <div class="panel-body" style="font-family:Calibri;">  
  <select class="custom-select" ng-class="{'no-border': toggle}" >
     <?php
     include("parameters.php");
     include("functions.php");

function chargerClasse($classe){
    require 'test/Class/'.$classe.'.php';
}
spl_autoload_register('chargerClasse');
     // Retrieve text periods from the database
     $sql = 'SELECT * FROM 2etexte_periode ORDER BY id_periode ASC';
     $req = mysqli_query($link, $sql)
     or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
     $i=1;
     $j=6;
     while($data = mysqli_fetch_assoc($req)){
        if($i==$j){
           echo "
           <option value=\"".$i."\" selected>".$data["nom_periode"]."</option>";
        }else{
           echo "
           <option value=\"".$i."\">".$data["nom_periode"]."</option>";     
        }
        $i+=1;
     }
     ?>
  </select>
  en rapport avec la notion 
  <select class="custom-select" ng-class="{'no-border': toggle}" >
     <?php
     // Retrieve notions from the database
     $sql = 'SELECT * FROM 2etexte_notion ORDER BY id_notion ASC';
     $req = mysqli_query($link, $sql)
     or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
     $i=1;
     $j=44; 
     while($data = mysqli_fetch_assoc($req)){
        if($i==$j){
           echo "
           <option value=\"".$i."\" selected>".$data["nom_det_notion"]."</option>";
        }else{
           echo "
           <option value=\"".$i."\">".$data["nom_det_notion"]."</option>";     
        }
        $i+=1;
     }
     ?>
  </select>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <button type="button" class="btn btn-primary" id="rechercheContenu" style="background-color:#8282D2">Rechercher</button></div>
</div>
-->


<!-- ARTICLE A LA UNE -->
<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Article à la une</h1>
  <hr/>
<?php
   //Load latest article
   $sql = 'SELECT * FROM 2etexte_article,2etexte_utilisatrice,2etexte_autrice WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_article.id_utilisatrice_article ORDER BY 2etexte_article.date_creation_article DESC LIMIT 1;';
   $req = mysqli_query($link, $sql)
      or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
   $data = mysqli_fetch_assoc($req);
   displayArticleAbstract($data["id_article"],$link);
?>
  </div>
</div>



<!-- ARTICLE A LA UNE -->
<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
<div class="col-lg-6 col-md-6">
  <h1 class="form-signin-heading">Derniers extraits ajoutés</h1>
  <hr/>
<?php 
   $managerE = new ExtraitManager($bdd); 
  $managerE->lastTexts(5);  
?>
  <a href="extraits.php">&rarr; tous les extraits disponibles...</a>
  <br/><br/>
</div>
<div class="col-lg-6 col-md-6">
  <h1 class="form-signin-heading">Derniers articles ajoutés</h1>
  <hr/>
<?php  
  lastArticles(5,$link);  
?>
</div>
  </div>
</div>

<div class="panel panel-body">
    <legend><h2 class="form-signin-heading">Le Deuxième Texte, c'est:</h2></legend>
    <?php
    $managerU = new UtilisatriceManager($bdd);
    $managerO = new OeuvreManager($bdd);
    $managerA = new AutriceManager($bdd);
    echo $managerU->statUtil();
    echo $managerE->statExtrait();
    echo $managerO->statOeuvre();
    echo $managerA->statAutrice();
    ?>
</div>
  
<?php
include("./footer.php");
?> 

</body>
</html>
