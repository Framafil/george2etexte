<br/>
<br/>
<br/>
<a name="about"></a>

<div class="footer" style="font-family:Calibri;">
<h1>À propos...</h1>
<p>
Le projet <i>George, le deuxième texte</i> est né
lors du <a href="https://forum.etalab.gouv.fr/t/a-propos-de-la-categorie-hackegalitefh/3445" target="_blank">premier
HackEgalitéFH</a>, dans l'espace de coworking <a href="http://letank.fr/">Le Tank</a>, du 3 au 5 mars 2017, par
<a href="https://twitter.com/clemencedouard">Clémence Douard</a>,
<a href="https://twitter.com/@CBrochette">Clémentine Brochier</a>,
Fil,
<a href="http://theses.fr/s112423">Anna-Livia Morand</a>
et <a href="http://igm.univ-mlv.fr/~gambette/">Philippe Gambette</a>,
avec la participation de Lauren Peuch.<br/>
<!--<a href="https://creativecommons.org/licenses/by-sa/2.0/fr/" target="_blank">CC BY-SA 2.0 FR</a>-->
</p>
<p>
Ce projet utilise <!--<a href="http://data.bnf.fr">Data BNF</a>--> 
<a href="https://www.wikidata.org">Wiki Data</a> pour récupérer les années de naissance et décès des autrices et auteurs recherchés,
et  pour identifier les autrices qui ont vécu à la même époque.
Divers <a href="https://forum.framasoft.org/viewtopic.php?f=37&t=39834&p=295650#p295650">outils collaboratifs Framasoft</a> ont été utilisés
pour ce projet lors du HackEgalitéFH.
</p>
<p>
Contact : george-2e-texte@gmx.fr
</p>
<br/>
<br/>
<br/>

</div>
</div>
</div>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-296895-5";
urchinTracker();
</script>
