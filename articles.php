<html>
<head>
   <title>Le deuxième texte - Articles</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/logo_le_deuxieme_texte-small.png" type="images/png" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("./parameters.php");
include("./functions.php");

if (isset($_GET["id"])) {
    $utilisatrice_article = " AND 2etexte_v1_article.id_utilisatrice_article=".intval($_GET["id"]);
} else {
    $utilisatrice_article = "";
}

$sql = 'SELECT * FROM 2etexte_v1_utilisatrice,2etexte_v1_article,2etexte_v1_oeuvre,2etexte_v1_autrice
WHERE 2etexte_v1_utilisatrice.id_utilisatrice=2etexte_v1_article.id_utilisatrice_article 
AND 2etexte_v1_article.ok_article=1'.$utilisatrice_article.' 
GROUP BY id_article ORDER BY date_modif_article DESC';
echo "<!-- ///SQL///SELECT * FROM 2etexte_v1_utilisatrice,2etexte_v1_article,2etexte_v1_oeuvre,2etexte_v1_autrice
WHERE 2etexte_v1_utilisatrice.id_utilisatrice=2etexte_v1_article.id_utilisatrice_article 
AND 2etexte_v1_article.ok_article=1".$utilisatrice_article."
GROUP BY id_article ORDER BY date_modif_article DESC -->";
?>

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Articles 
  <?php
  if(strlen($utilisatrice_article)>0){
     $req = mysqli_query($link, "SELECT * FROM 2etexte_v1_utilisatrice 
     WHERE 2etexte_v1_utilisatrice.id_utilisatrice=".intval($_GET["id"]))
     or die('Erreur SQL !<br>');
     $data = mysqli_fetch_assoc($req);
     echo " proposés par ".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"]);
  } else {
     echo " disponibles";
  }
  ?></h1>
  <hr/>
<?php

  // Informations sur l'utilisatrice
  if(strlen($utilisatrice_article)>0){
     echo "<p>";
     displayUser($data);
     echo "<br/><br/></p>";
  }

  // Informations sur l'autrice
  if(strlen($id_autrice)>0){
     echo "<p>";
     displayAuthor($data);
     echo "<br/><br/></p>";
  }  
  
  // Affiche tous les articles, en supprimant la colonne contributrice si la contributrice est connue
  allArticles($link,$sql,strlen($utilisatrice_article)==0);
?>
  </div>
</div>

  
<?php
include("./footer.php");
?> 
</body>
</html>
