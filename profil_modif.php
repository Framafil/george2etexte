<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="dist/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/normalize.css">
    <title>Modification du profil</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "test/Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    session_start();
    include('parameters.php');
    
    $manager = new UtilisatriceManager($bdd);
    $utilisatrice = $manager->getUtilisatriceById($_SESSION['id']);
    ?>
    <a href="profil_prive.php">Retour</a>
    <form action="profil_modif_donnees.php" method="post" enctype="multipart/form-data">
        <fieldset><legend>Modifiez votre profil: </legend>
          <?php
            echo $utilisatrice->afficheFormulaire();
            ?>
           <label for="mdpin">Mot de passe</label><input type="password" name="mdpin" id="mdpin" pattern="^.{8,}$"><br><br>
           <label for="confirmation">Confirmez votre mot de passe</label><input type="password" name="confirmation" id="confirmation" pattern="^.{8,}$"><br><br><div id="noconfirmation"></div>
            <input type="submit" value="Modifier" id="envoiinscription">
        </fieldset>
    </form>
    <script>
       var ecrit = document.querySelector('#ecrit');
       var textarea = document.getElementById('presentation');
        var caracteres = 500 - textarea.value.length;
           if(caracteres <= 1){
               ecrit.textContent = caracteres + " caractère restant";
           } else {
               ecrit.textContent = caracteres + " caractères restants";
           }
       textarea.addEventListener('keyup', function(){
           var caracteres = 500 - textarea.value.length;
           if(caracteres <= 1){
               ecrit.textContent = caracteres + " caractère restant";
           } else {
               ecrit.textContent = caracteres + " caractères restants";
           }
       });
    </script>
   <script src="js/jquery-3.2.1.min.js"></script>
   <script>
    var paragphoto = document.querySelector('.profil'),
    mdp1 = document.getElementById('mdpin'), 
        mdp2 = document.getElementById('confirmation'), 
        envoi = document.getElementById('envoiinscription'),
        nomail = document.getElementById('nomail'),
        nolien = document.getElementById('nolien'),
        noconfirmation = document.getElementById('noconfirmation'),
        checkbox = document.getElementById('avatar');
       mdp2.disabled = true;
       
       //A mettre dans mdp1.addEventListener
       
       
        
        function verifMdp(valeur1, valeur2){
            if(valeur1 !== valeur2 && valeur1 !== "" && valeur2 !== ""){
               noconfirmation.innerHTML = "<p>Le mot de passe de confirmation ne correspond pas au mot de passe précédemment tapé</p>";
            } else {
                noconfirmation.innerHTML = "";
            }
        }
        
        function verifInscription(){
            if(nomail.textContent !== "" || nolien.textContent !== "" || noconfirmation.textContent !== ""){
                           envoi.disabled = true;
                       } else {
                           envoi.disabled = false;
                       }
        }
       
       function insertionAjax(div, code){
            if(code == ""){
                $(div).children('p').replaceWith("");
            } else {
                $(div).children('p').replaceWith("");
                $(code).appendTo(div);
            }
        }
       
       mdp1.addEventListener('blur', function(e){
           if(mdp1.value !== ""){
                mdp2.required = true;
                mdp2.disabled = false;
            } else {
                mdp2.required = false;
                mdp2.disabled = true;
            }
           verifMdp(mdp1.value, mdp2.value);
            verifInscription();
        });
        
        mdp2.addEventListener('blur', function(e){
           verifMdp(mdp1.value, mdp2.value);
            verifInscription();
        });
       
       checkbox.addEventListener('click', function(){
            if(checkbox.checked == true){
                paragphoto.innerHTML = "Vous pouvez modifier votre photo de profil si vous le souhaitez:";
            } else {
                var photography = document.createElement('input');
                photography.type = "file";
                photography.name = "photo";
                photography.id = "photo";
                photography.accept = "image/jpeg";
                paragphoto.appendChild(photography);
            }
        });
        
$(document).ready(function(){
    $('#mailing').blur(function(){
                var param = "m=" + $('#mailing').val() + "&s=" + <?php echo $_SESSION['id']; ?>;
               $.ajax({
                  url: 'test/ajax/no_mail_3.php',
                   type: 'GET',
                   data: param,
                   success: function(code, statut){
                       insertionAjax('#nomail', code);
                       verifInscription();
                   }
               });
    });
         $('#lien_site').blur(function(){
                var param2 = "t=" + $('#lien_site').val() + "&l=" + <?php echo $_SESSION['id']; ?>;
               $.ajax({
                  url: 'test/ajax/no_website_2.php',
                   type: 'GET',
                   data: param2,
                   success: function(code, statut){
                       insertionAjax('#nolien', code);
                       verifInscription();
                   }
               });
    });
});

    </script>
</body>
</html>