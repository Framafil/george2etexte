<html>
<head>
   <title>George, le deuxième texte - Inscription au beta-test de la plateforme</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">

<?php
include("header.php");
?>

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body" style="font-family:Calibri;font-size:14pt;">
  <p>
  <i><a href="./index.php">George - Le deuxième texte</a></i> est une plateforme web,
  conçue lors du <a href="https://forum.etalab.gouv.fr/t/a-propos-de-la-categorie-hackegalitefh/3445">#HackEgalitéFH</a>,<br/>
  qui vise à <b>mettre à disposition des professeur·e·s des textes écrits par des femmes,<br/>
  avec du contenu pédagogique associé</b>.
  </p>
  <p>
  L’objectif est de donner plus de visibilité aux autrices dans les programmes scolaires,<br/>
  afin que les jeunes puissent s’identifier à des figures fortes, sans distinction de genre.
  </p>
  </div>
</div>



<div style="font-size:20pt;text-align:center" class="titresPage"><i>George, le deuxième texte</i>, est en cours de construction...</div>
<div class="panel panel-default" style="text-align:center;padding:20px;margin-top:-10px;">
  <p>... et nous cherchons des enseignantes et enseignants pour <b>tester la plateforme</b> et <b>envoyer des extraits de textes de femmes</b> !</p>
  <p>Si vous souhaitez simplement nous <b>envoyer des extraits de textes de femmes</b> que vous avez déjà utilisés en classe, vous pouvez aussi nous envoyer directement <b>un courriel à l'adresse <a href="mailto:contact@ledeuxiemetexte.fr">contact@ledeuxiemetexte.fr</a></b>.</p>
  <!--Ancien formulaire général de disponibilité pour contribuer aux tests de la plateforme : <iframe src="http://framaforms.org/contribuer-a-george-le-deuxieme-texte-1490602110" width="100%" height="1000" border="0" ></iframe>  -->
  <iframe src="https://framaforms.org/suggestion-dextraits-pour-la-plateforme-le-deuxieme-texte-1654881604" width="100%" height="1000" border="0" ></iframe>
  <p></p>
  <p><br/></p>
  <p>N'hésitez pas à nous contacter si vous avez des questions à propos du projet,<br/>notre adresse mail est indiquée au bas de cette page !</p>
</div>





  
<?
include("footer.php");
?> 

</body>
</html>
