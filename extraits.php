<html>
<head>
   <title>Le deuxième texte - Extraits</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/logo_le_deuxieme_texte-small.png" type="images/png" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("./parameters.php");
include("./functions.php");

if (isset($_GET["id"])) {
    $utilisatrice_extrait = " AND 2etexte_v1_extrait.utilisatrice_extrait=".intval($_GET["id"]);
} else {
    $utilisatrice_extrait = "";
}

if (isset($_GET["autId"])) {
    // Restrict to a single author
    if(substr($_GET["autId"], 0, 1) == "Q"){
        $id_autrice = " AND 2etexte_v1_autrice.id_wikidata='Q".intval(substr($_GET["autId"], 1))."'";
    } else {
        $id_autrice = " AND 2etexte_v1_autrice.id_autrice=".intval($_GET["autId"]);
    }
} else {
    // Display all excerpts of the database
    $id_autrice = "";
}

if (isset($_GET["jourSansE"])) {
    $jourSansE = " AND (2etexte_v1_autrice.nom_autrice='Cixous' OR 2etexte_v1_autrice.nom_autrice='Duras' OR 2etexte_v1_autrice.nom_autrice='Goby' OR 2etexte_v1_autrice.nom_autrice='Pozzi' OR 2etexte_v1_autrice.nom_autrice='Sand')";
} else {
    $jourSansE = "";
}

$sql = 'SELECT * FROM 2etexte_v1_utilisatrice,2etexte_v1_extrait,2etexte_v1_oeuvre,2etexte_v1_autrice
WHERE 2etexte_v1_utilisatrice.id_utilisatrice=2etexte_v1_extrait.utilisatrice_extrait AND 2etexte_v1_extrait.oeuvre_extrait=2etexte_v1_oeuvre.id_oeuvre AND 2etexte_v1_oeuvre.id_autrice_oeuvre=2etexte_v1_autrice.id_autrice 
AND 2etexte_v1_extrait.ok_extrait=1'.$utilisatrice_extrait.$id_autrice.$jourSansE.' 
GROUP BY id_extrait ORDER BY nom_autrice ASC';
?>

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Extraits 
  <?php
  if(strlen($utilisatrice_extrait)>0){
     $req = mysqli_query($link, "SELECT * FROM 2etexte_v1_utilisatrice 
     WHERE 2etexte_v1_utilisatrice.id_utilisatrice=".intval($_GET["id"]))
     or die('Erreur SQL !<br>');
     $data = mysqli_fetch_assoc($req);
     echo " proposés par ".fullName($data["prenom_utilisatrice"],$data["nom_utilisatrice"]);
  } else {
     if(strlen($id_autrice)>0){
        $req = mysqli_query($link, "SELECT * FROM 2etexte_v1_extrait,2etexte_v1_oeuvre,2etexte_v1_autrice 
        WHERE 2etexte_v1_extrait.oeuvre_extrait=2etexte_v1_oeuvre.id_oeuvre AND 2etexte_v1_oeuvre.id_autrice_oeuvre=2etexte_v1_autrice.id_autrice".$id_autrice)
        or die('Erreur SQL !<br>');
        $data = mysqli_fetch_assoc($req);
        echo " écrits par ".fullName($data["prenom_autrice"],$data["nom_autrice"]);
     } else {
        if(strlen($jourSansE)>0){
           echo " disponibles (noms d'autrices sans la lettre e)";
        } else { 
           echo " disponibles";
        }
     }
  }
  ?></h1>
  <hr/>
<?php

  // Informations sur l'utilisatrice
  if(strlen($utilisatrice_extrait)>0){
     echo "<p>";
     displayUser($data);
     echo "<br/><br/></p>";
  }

  // Informations sur l'autrice
  if(strlen($id_autrice)>0){
     echo "<p>";
     displayAuthor($data);
     echo "<br/><br/></p>";
  }  
  
  // Affiche tous les articles, en supprimant la colonne contributrice si la contributrice est connue
  allTexts($link,$sql,strlen($utilisatrice_extrait)==0);
?>
  </div>
</div>

  
<?php
include("./footer.php");
?> 
</body>
</html>
